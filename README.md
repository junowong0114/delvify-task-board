# Task board app for Delvify case study

## Welcome
  Task Board is made for Delvify case study.    
      
  The frontend is written with React.js (Typescript) and react-bootstrap framework, allowing it to be fully mobile responsive.    
      
  The web server is written in NodeJS (Typescript) on Express framework.    
      
  The database server is a dockerize PostgreSQL database. Communication between web server and database is done by knex.js query builder.    

## Check list
### Basic    
- [x] User can view all tasks and lists
- [x] User can create empty list with name property
- [x] User can delete entire list with all its tasks
- [x] User can add new tasks to existing list with name, description, and deadline property
- [x] User can update all properties of a task
- [x] User can move a single task to another list
- [x] User can move multiple tasks to another list in a single operation
- [x] User can delete multiple tasks in one transaction
- [x] User can complete a task
- [ ] User will receive email upon task completion
- [ ] User will receive email notification upon task overdue    

### Bonus    
- [x] Containerization with docker-compose. *Only database server
- [x] Relational database (PostgreSQL)
- [x] Good onboarding steps
- [ ] Test: only minimal tests on UserService, UserController, and some helper function. Tests are not maintained.
- [ ] API documentation: Although documentation is not explicitly made, I can refer to the <a href="https://gitlab.com/junowong0114/delvify-task-board/-/blob/main/shared/src/types.ts">shared module</a> in the future when I am required to make a doc. The format of response and request bodies for different APIs is recorded under API namespace.    

### Extra    
- [x] Auth system with JSON web token.
- [x] Mobile responsiveness
- [x] Local storage persistent data.
- [x] MVC structure in API server
- [x] Shared types between frontend, API server, and database

## Set up
1. Clone / fork this remote repository
2. Pre-requisite  
    You must have the latest NodeJS installed  
    You must have latest docker installed  
    
3. Create .env file in react-app directory, example:
    ```
    NODE_ENV=development
    DISABLE_ESLINT_PLUGIN=true
    ```
4. Create .env file in web-server directory. example:
    ```
    # node environment
    NODE_ENV=development

    # PORT number for web server
    PORT=8080

    # others
    JWT_SECRET=delvifycodetest

    # pg database
    DEV_DB_NAME=task_board
    DEV_DB_USER=postgres
    DEV_DB_PASSWORD=postgres

    TEST_DB_NAME=task_board_test
    TEST_DB_USER=postgres
    TEST_DB_PASSWORD=postgres

    POSTGRES_DB=task_board
    POSTGRES_USER=postgres
    POSTGRES_PASSWORD=postgres
    POSTGRES_HOST='localhost'
    ```
5. In the root directory of this repo, run the following:
    ```
    docker-compose up

    cd ./web-server
    npm install
    npm run db-prepare && npm run dev

    cd ../react-app
    yarn install
    yarn start
    ```
6. If you don't want to do any of the above, you can just run the following in the root directory of this repo:
    ```
    chmod +x ./scripts/*
    chmod +x ./scripts/local/*
    ./scripts/db-server-init.sh
    ```
    open another new terminal
    ```
    ./scripts/web-server-init.sh
    ```
    open another new terminal
    ```
    ./scripts/react-app-init.sh
    ```
    and we are set   
    If the app didn't pop up, access it through localhost:3000/
    
7. Login to the app with one of the following credentials:
    ```
    Email address: admin@gmail.com
    Password: admin

    Email address: user@gmail.com
    Password: user
    ```
## Other Issue   
  - The docker postgres container uses PORT 6432  
  - The web server uses PORT 8800  
  - The react app uses PORT 3000  
  - Make sure all of those ports are not occupied  
  - Please make sure you kill the docker container by pressing Cltr+C / Command+C afterwards
