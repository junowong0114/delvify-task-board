export type JWTPayload = {
  id: number,
  email: string,
}

export namespace DB {
  export type user = {
    id: number,
    email: string,
    hash_password: string,
    created_at: string, // ISO date string
    updated_at: string, // ISO date string
  }

  export type task = {
    id: number,
    task_list_id: number,
    owner_id: number,
    task_name: string,
    description: string,
    completed: boolean,
    deadline: string, // ISO date string
    created_at: string, // ISO date string
    updated_at: string, // ISO date string
  }

  export type task_list = {
    id: number,
    owner_id: number,
    task_list_name: string,
    created_at: string, // ISO date string
    updated_at: string, // ISO date string
  }
}

export namespace DTO {
  export type Task = {
    taskId: number,
    taskName: string,
    description: string,
    deadline: string, // ISO date string
    completed: boolean,
  }

  export type TaskList = {
    ownerId: number,
    taskListId: number,
    taskListName: string,
    tasks: DTO.Task[],
  }
}

export namespace API {
  export type Response = {
    success: boolean,
  }

  // CRUD response format
  export type PostResponse = Response & {
    error?: string,
  }
  export type DelResponse = PostResponse
  export type PutResponse = PostResponse
  export type GetResponse<D> = Response & ({
    result: D,
  } | {
    error: string,
  })

  /* /register API format */
  export type PostRegisterResponse = PostLoginResponse
  export type PostRegisterBody = PostLoginBody

  /* /login API format */
  export type PostLoginResponse = Response & ({
    token: string,
  } | {
    error: string,
  })
  export type PostLoginBody = {
    email: string,
    password: string,
  }

  /* /task_lists API body format */
  export type PostTaskListsBody = {
    taskLists: Array<Omit<DTO.TaskList, 'ownerId' | 'taskListId' | 'tasks'> & {
      tasks: Omit<DTO.Task, 'taskId' | 'completed'>[]
    }>
  }
  export type DelTaskListsBody = {
    taskListIds: number[],
  }
  
  /* /task_list API body format */
  export type PutTaskListBody = {
    taskList: (Partial<Omit<DTO.TaskList, 'ownerId' | 'tasks'>> & {
      taskListId: number,
      tasks?: PutTasksBody['tasks']
    })
  }

  /* /tasks API body format */
  export type PostTasksBody = {
    taskListId: number
    tasks: Omit<DTO.Task, 'taskId' | 'completed'>[]
  }
  export type PutTasksBody = {
    tasks: Array<Partial<DTO.Task> & {
      taskId: number,
      taskListId?: number,
    }>
  }
  export type DeleteTasksBody = {
    taskIds: number[]
  }
}