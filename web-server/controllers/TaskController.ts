import { Request, Response } from 'express'
import { API, JWTPayload, errorToString } from 'shared'
import TaskService from '../services/TaskService'
import { BadRequestError, getErrorStatus } from '../helpers/error'
import { UpdateTaskData } from '../helpers/types'
import { removeEmptyValue } from '../helpers/format'
import { userService } from '../app'

export default class TaskController {
  constructor(private taskService: TaskService) { }

  deleteTasks = async (req: Request, res: Response) => {
    let resBody: API.DelResponse

    try {
      const { taskIds } = req.body as API.DeleteTasksBody
      if (!taskIds || !taskIds.length) throw new BadRequestError('Missing taskIds in request body.')

      /* validate user */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists

      /* database operation */
      await this.taskService.deleteTasks(taskIds)

      /* response to user */
      resBody = {
        success: true
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  createTasks = async (req: Request, res: Response) => {
    let resBody: API.PostResponse

    try {
      /* obtain parameters */
      const { taskListId, tasks } = req.body as API.PostTasksBody
      if (!taskListId) throw new BadRequestError('Missing taskListId in request body.')
      if (!tasks || !tasks.length) throw new BadRequestError('Missing tasks in request body.')

      /* validate user */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists

      /* database operation */
      await this.taskService.createTasks(jwtPayload.id, taskListId, tasks)

      /* response to user */
      resBody = {
        success: true
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  updateTasks = async (req: Request, res: Response) => {
    let resBody: API.PutResponse

    try {
      /* obtain parameters */
      const { tasks } = req.body as API.PutTasksBody
      if (!tasks) throw new BadRequestError('Missing tasks in request body.')

      /* validate user */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists

      /* database operation */
      const infoForService: UpdateTaskData[] = tasks.map(row => {
        if (!row.taskId) throw new BadRequestError('Missing taskId.')
        return removeEmptyValue({
          id: row.taskId,
          task_list_id: row.taskListId,
          task_name: row.taskName,
          description: row.description,
          deadline: row.deadline,
          completed: row.completed,
        })
      })

      await this.taskService.updateTasks(infoForService)

      /* response to user */
      resBody = {
        success: true
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }
}