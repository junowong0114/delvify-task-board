import { Request, Response } from 'express'
import { API, errorToString } from 'shared'
import { BadRequestError, getErrorStatus } from '../helpers/error'
import UserService from '../services/UserService'

export default class UserController {
  constructor(private userService: UserService) { }

  createUser = async (req: Request, res: Response<API.PostLoginResponse>) => {
    let token: string
    let resBody: API.PostRegisterResponse
    
    try {
      const { email, password } = req.body as API.PostRegisterBody
      if (!email || !password) throw new BadRequestError('Missing email/password in req.body')

      token = await this.userService.createUser(email, password)
      resBody = {
        success: true,
        token
      }
      res.status(200).json(resBody)
    } catch (e) {
      console.log(e instanceof BadRequestError)
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  loginWithPassword = async (req: Request, res: Response<API.PostLoginResponse>) => {
    const { email, password } = req.body
    let token: string
    
    try {
      if (!email || !password) throw new BadRequestError('Missing email/password in req.body')

      token = await this.userService.loginWithPassword(email, password)
      res.status(200).json({
        success: true,
        token
      })
    } catch (e) {
      res.status(getErrorStatus(e)).json({
        success: false,
        error: errorToString(e)
      })
      return
    }
  }
}