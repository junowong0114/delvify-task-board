import UserService from '../services/UserService'
import UserController from './UserController'
import { Knex } from 'knex'
import { Request, Response } from 'express'

describe('UserController', () => {
  let userController: UserController
  let userService: UserService
  let req: Request
  let res: Response

  beforeEach(() => {
    userService = new UserService({} as Knex)
    res = {
      json: jest.fn(() => null),
      status: jest.fn(() => res)
    } as any as Response

    userController = new UserController(userService)
  })

  it('should return token when login success', async () => {
    jest.spyOn(userService, 'loginWithPassword').mockImplementation(async () => '123')
    req = {
      body: {
        email: "admin@gmail.com",
        password: "admin"
      }
    } as Request

    await userController.loginWithPassword(req, res)
    expect(userService.loginWithPassword).toBeCalledWith("admin@gmail.com", "admin")
    expect(res.status).toBeCalledWith(200)
    expect(res.status(200).json).toBeCalledWith({ success: true, token: '123' })
  })

  it('should handle error when email is missing from req.body', async () => {
    jest.spyOn(userService, 'loginWithPassword').mockImplementation(async () => '123')
    req = {
      body: {
        password: "admin",
      }
    } as Request

    await userController.loginWithPassword(req, res)
    expect(userService.loginWithPassword).not.toBeCalled()
    expect(res.status).toBeCalledWith(400)
    expect(res.status(400).json).toBeCalledWith({ success: false, error: 'Missing email/password in req.body' })
  })

  it('should handle error when password is missing from req.body', async () => {
    jest.spyOn(userService, 'loginWithPassword').mockImplementation(async () => '123')
    req = {
      body: {
        email: "admin@gmail.com",
      }
    } as Request

    await userController.loginWithPassword(req, res)
    expect(userService.loginWithPassword).not.toBeCalled()
    expect(res.status).toBeCalledWith(400)
    expect(res.status(400).json).toBeCalledWith({ success: false, error: 'Missing email/password in req.body' })
  })

  it('should handle any error from userService', async () => {
    jest.spyOn(userService, 'loginWithPassword').mockImplementation(() => Promise.reject('generic error'))
    req = {
      body: {
        email: "admin@gmail.com",
        password: "admin"
      }
    } as Request

    await userController.loginWithPassword(req, res)
    expect(res.status).toBeCalledWith(400)
    expect(res.status(400).json).toBeCalledWith({ success: false, error: 'generic error' })
  })
})