import { Request, Response } from 'express'
import { API, DTO, JWTPayload, errorToString } from 'shared'
import TaskListService from '../services/TaskListService'
import { BadRequestError, getErrorStatus, UnauthorizedError } from '../helpers/error'
import { userService } from '../app'
import { pgISODateToJsISODate, removeEmptyValue } from '../helpers/format'
import { UpdateTaskData, UpdateTaskListData } from '../helpers/types'

export default class TaskListController {
  constructor(private taskListService: TaskListService) { }

  updateTaskList = async (req: Request, res: Response) => {
    let resBody: API.PutResponse

    try {
      /* obtain parameters */
      const { taskList } = req.body as API.PutTaskListBody
      if (!taskList) throw new BadRequestError('Missing taskList in request body')

      /* validate user */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists

      /* format data */
      const updateTaskListBody: Omit<UpdateTaskListData, 'id'> = removeEmptyValue({
        task_list_name: taskList.taskListName,
      })

      let updateTasksBody: UpdateTaskData[] | undefined
      if (taskList.tasks) {
        updateTasksBody = taskList.tasks.map(row => {
          if (!row.taskId) throw new BadRequestError('Missing taskId.')
          return removeEmptyValue({
            id: row.taskId,
            task_list_id: row.taskListId,
            task_name: row.taskName,
            description: row.description,
            deadline: row.deadline,
            completed: row.completed,
          })
        })
      }

      /* database operation */
      await this.taskListService.updateTaskList(taskList.taskListId, updateTaskListBody, updateTasksBody)

      /* response to user */
      resBody = {
        success: true
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  createTaskLists = async (req: Request, res: Response) => {
    let resBody: API.PostResponse

    try {
      /* obtain parameters */
      const { taskLists } = req.body as API.PostTaskListsBody
      if (!taskLists || !taskLists.length) throw new BadRequestError('Missing taskLists in request body.')

      /* database operation */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists
      await this.taskListService.createTaskLists(jwtPayload.id, taskLists)

      /* response to user */
      resBody = {
        success: true
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  getTaskList = async (req: Request, res: Response) => {
    let resBody: API.GetResponse<DTO.TaskList>

    try {
      /* obtain parameters */
      const taskListId = parseInt(req.params.taskListId)
      if (isNaN(taskListId)) throw new BadRequestError('Task list id must be a number.')

      /* database operations */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists
      const rawData = await this.taskListService.getTaskList(taskListId)

      /* format data from service */
      const taskList: DTO.TaskList = {
        ownerId: rawData.owner_id,
        taskListId: rawData.task_list_id,
        taskListName: rawData.task_list_name,
        tasks: rawData.tasks[0].taskId === null ? [] : rawData.tasks.map((row: DTO.Task) => {
          return {
            ...row,
            // IMPORTANT: Must add 'Z' to ISOstring from pg
            deadline: pgISODateToJsISODate(row.deadline)
          }
        })
      }
      if (taskList.ownerId !== jwtPayload.id) {  // check if task list belongs to the requesting user
        throw new UnauthorizedError(`Unauthorized access on task list ${taskListId}`)
      }

      /* response */
      resBody = {
        success: true,
        result: taskList
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  getTaskLists = async (req: Request, res: Response) => {
    let resBody: API.GetResponse<DTO.TaskList[]>

    try {
      /* database operation */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      const [dbUserId] = await userService.validateUser(jwtPayload.id) // check if user exists
      const rawData = await this.taskListService.getTaskLists(dbUserId)

      /* format data from service */
      const taskLists: DTO.TaskList[] = []
      for (let row of rawData) {
        taskLists.push({
          ownerId: row.owner_id,
          taskListId: row.task_list_id,
          taskListName: row.task_list_name,
          tasks: row.tasks[0].taskId === null ? [] :
            row.tasks.map((row: DTO.Task) => {
              return {
                ...row,
                // IMPORTANT: Must add 'Z' to ISOstring from pg
                deadline: pgISODateToJsISODate(row.deadline)
              }
            })
        })
      }

      /* response to user */
      resBody = {
        success: true,
        result: taskLists
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }

  deleteTaskLists = async (req: Request, res: Response) => {
    const { taskListIds } = req.body
    let resBody: API.DelResponse

    try {
      /* validate input parameters */
      if (!taskListIds || !taskListIds.length) throw new BadRequestError('Missing task list ids from body')

      /* database operation */
      const jwtPayload = req.jwtPayload as JWTPayload // guarded by middleware to be defined
      await userService.validateUser(jwtPayload.id) // check if user exists
      await this.taskListService.deleteTaskLists(taskListIds)

      /* response to user */
      resBody = {
        success: true,
      }
      res.status(200).json(resBody)
    } catch (e) {
      resBody = {
        success: false,
        error: errorToString(e)
      }
      res.status(getErrorStatus(e)).json(resBody)
    }
  }
}