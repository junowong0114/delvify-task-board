export function getPort(): number {
  const PORT = process.env.PORT
  if (!PORT) throw new Error('PORT not found in .env file')

  const intPORT = parseInt(PORT)
  if (parseInt(PORT) === NaN) throw new Error('PORT in .env file must be a number')
  
  return intPORT
}