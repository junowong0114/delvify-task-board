import dotenv from 'dotenv';

dotenv.config();

function getJWTSecret() {
  const JWT_SECRET = process.env.JWT_SECRET
  if (!JWT_SECRET) throw new Error('missing JWT_SECRET in .env')
  return JWT_SECRET
}

export const jwtConfig = {
  SECRET: getJWTSecret()
}