export class UnauthorizedError extends Error {
  constructor(msg: string) {
    super(msg)
    Object.setPrototypeOf(this, UnauthorizedError.prototype)
  }
}

export class BadRequestError extends Error {
  constructor(msg: string) {
    super(msg)
    Object.setPrototypeOf(this, BadRequestError.prototype)
  }
}

export class DuplicationError extends Error {
  constructor(msg: string) {
    super(msg)
    Object.setPrototypeOf(this, DuplicationError.prototype)
  }
}

export function getErrorStatus(e: any) {
  if (e instanceof UnauthorizedError) return 401
  if (e instanceof BadRequestError) return 400
  if (e instanceof DuplicationError) return 409
  return 500
}