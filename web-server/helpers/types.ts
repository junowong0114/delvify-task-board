import { DB, JWTPayload } from 'shared';
export * from 'shared';

declare global {
  namespace Express {
    interface Request {
      jwtPayload?: JWTPayload
    }
  }
}

export type UpdateTaskData = Partial<Omit<DB.task, 'created_at' | 'updated_at' | 'owner_id'>> & Pick<DB.task, 'id'>
export type UpdateTaskListData = Partial<Omit<DB.task_list, 'created_at' | 'updated_at' | 'owner_id'>> & {
  id: number
}
