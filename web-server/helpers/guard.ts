import { Request, Response, NextFunction } from 'express';
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import { jwtConfig } from './jwt';
import { JWTPayload } from './types';

const permit = new Bearer({
  query: "access_token"
});

function retrieveToken(req: Request) {
  let token: string
  try {
    token = permit.check(req);
  } catch (e) { 
    throw new Error('Failed to decode Bearer token in req.')
  }

  if (!token) {
    throw new Error('Missing Bearer token in req.')
  }
  
  return token
}

function retrievePayload(token: string) {
  let payload: JWTPayload;
  try {
    payload = jwtSimple.decode(token, jwtConfig.SECRET);
  } catch (e) {
    throw new Error('Failed to decode JWT token in req')
  }

  return payload
}

export function requireJWT(req: Request, res: Response, next: NextFunction) {
  let token: string
  let payload: JWTPayload

  try {
    token = retrieveToken(req)
    payload = retrievePayload(token)
  } catch (e) {
    res.status(401).json({
      success: false,
      error: (e as Error).toString()
    })
    return
  }

  req.jwtPayload = payload;
  next();
}