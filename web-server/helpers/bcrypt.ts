import { hash, compare } from 'bcryptjs'

const SALT_ROUNDS = 10

export function hashPassword(password: string): Promise<string> {
  return hash(password, SALT_ROUNDS)
}

export function comparePassword(
  password: string,
  password_hash: string,
): Promise<boolean> {
  return compare(password, password_hash)
}