export function pgISODateToJsISODate(pgISOString: string) {
  return pgISOString + 'Z'
}

export function removeEmptyValue<Input extends Object, Output extends Partial<Input>>(obj: Input) {
  return (Object.entries(obj)
    .filter(([_, v]) => !(typeof v === 'undefined'))
    .reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {})) as Output
}