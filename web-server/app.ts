// server
import express, { Request, Response } from 'express'
import { print } from 'listening-on'
import cors from 'cors'

// env
import dotenv from 'dotenv'
import { getPort } from './helpers/env'
import { knex } from './db'

// MVC components
import UserService from './services/UserService'
import UserController from './controllers/UserController'
import TaskService from './services/TaskService'
import TaskController from './controllers/TaskController'
import TaskListService from './services/TaskListService'
import TaskListController from './controllers/TaskListController'

dotenv.config()

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

/* controllers and services */
export const userService = new UserService(knex)
export const userController = new UserController(userService)

export const taskService = new TaskService(knex)
export const taskController = new TaskController(taskService)

export const taskListService = new TaskListService(knex)
export const taskListController = new TaskListController(taskListService)

import routes from './routes'
app.use(routes)
app.get('/', (req: Request, res: Response) => {
  res.end('Welcome to TaskBoard\'s API server.')
})
// fall back route
app.use((req: Request, res: Response) => {
  res.status(404).json({
    error: `${req.method} ${req.path} is not allowed.`
  })
})

const PORT = getPort()
app.listen(PORT, () => {
  print(PORT)
})