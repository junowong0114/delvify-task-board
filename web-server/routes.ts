import express from 'express'

import {
  userController,
  taskController,
  taskListController,
} from './app'
import { requireJWT } from './helpers/guard'

const routes = express.Router()

/* user routes */
routes.post('/login', userController.loginWithPassword)
routes.post('/register', userController.createUser)

/* task lists routes */
routes.post('/task_lists', requireJWT, taskListController.createTaskLists)
routes.get('/task_lists', requireJWT, taskListController.getTaskLists)
routes.delete('/task_lists', requireJWT, taskListController.deleteTaskLists)

/* task list route */
routes.get('/task_list/:taskListId', requireJWT, taskListController.getTaskList)
routes.put('/task_list', requireJWT, taskListController.updateTaskList)

/* tasks route */
routes.post('/tasks', requireJWT, taskController.createTasks)
routes.put('/tasks', requireJWT, taskController.updateTasks)
routes.delete('/tasks', requireJWT, taskController.deleteTasks)

export default routes