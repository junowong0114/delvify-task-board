export function errorToString(error: any | Error) {
  return (error as Error).toString().replace(/Error: /g, '')
}