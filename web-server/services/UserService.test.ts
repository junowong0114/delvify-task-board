import UserService from './UserService'
import Knex from 'knex'
import { jwtConfig } from '../helpers/jwt'
import jwtSimple from 'jwt-simple'
import { DB, JWTPayload } from 'shared'
import { hashPassword } from '../helpers/bcrypt'

const knexfile = require('../knexfile')
const knex = Knex(knexfile["test"])

describe('UserService', () => {
  let userService: UserService
  let [email, password] = ['admin@gmail.com', 'admin']
  let id: number

  beforeAll(async () => {
    await knex<DB.user>('user').del()
    const result = await knex<DB.user>('user').insert({
      email,
      hash_password: await hashPassword(password)
    }).returning(['id'])

    if (!result || !result.length) throw new Error('Unknown error during UserService test.')
    id = result[0].id
  })

  beforeEach(() => {
    userService = new UserService(knex)
  })

  it('should extract token from database', async () => {
    const token = await userService.loginWithPassword(email, password)

    await knex<DB.user>('user').select('id').where('email', email).first()
    let payload: JWTPayload = {
      id,
      email,
    }
    expect(token).toBe(jwtSimple.encode(payload, jwtConfig.SECRET))
  })

  it('should throw error if email address is not found in database', async () => {
    expect.assertions(1)
    try {
      await userService.loginWithPassword('abc@gmail.com', password)
    } catch (e) {
      expect(e.toString()).toMatch('Incorrect email address.')
    }
  })

  it('should throw error if password is incorrect', async() => {
    expect.assertions(1)
    try {
      await userService.loginWithPassword(email, 'admin1')
    } catch (e) {
      expect(e.toString()).toMatch('Incorrect email address or password')
    }
  })

  afterAll(() => {
    knex.destroy()
  })
})