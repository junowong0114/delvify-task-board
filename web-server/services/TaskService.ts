import { Knex } from 'knex'
import { API, DB } from 'shared'
import { UnauthorizedError, BadRequestError } from '../helpers/error'
import { UpdateTaskData } from '../helpers/types'

export default class TaskService {
  constructor(private knex: Knex) { }

  getTasks = async (taskIds: number[]) => {
    const getTasksQueries = taskIds.map(id => {
      return this.knex<DB.task>('task')
        .select('*')
        .where('id', id)
    })

    const res = await Promise.all(getTasksQueries)
    if (!res || res.length !== taskIds.length) throw new BadRequestError('Get tasks failed')

    return res.map(row => row[0])
  }

  deleteTasks = async (taskIds: number[]) => {
    const deleteTaskQueries = taskIds.map(id => {
      return this.knex<DB.task>('task')
        .where('id', id)
        .del()
    })

    await Promise.all(deleteTaskQueries)
  }

  updateTasks = async (data: UpdateTaskData[]) => {
    /* validate tasks */
    const tasksExistQueries = data.map(row => {
      return (this.knex<DB.task>('task')
        .select('id', 'task_list_id')
        .where('id', row.id))
    })
    const tasksExistRes = await Promise.all(tasksExistQueries)
    // if some task does not exists
    if (tasksExistRes.some(row => !row) || tasksExistRes.some(row => !row.length)) {
      throw new BadRequestError(`One or more tasks does not exist.`)
    }

    /* obtain ids of tasks and taskLists to be updated */
    // dbTaskListIds is obtained for moving tasks to another task list
    const [dbTaskIds, dbTaskListIds] = tasksExistRes.reduce((res, row) => {
      const rowTaskId = row[0].id
      const rowTaskListId = row[0].task_list_id
      return [
        [
          ...res[0],
          rowTaskId,
        ],
        [
          ...res[1],
          rowTaskListId,
        ]
      ]
    }, [[], []])

    /* Update updated_at column in task_list table */
    const dbTaskListIdsSet = new Set(dbTaskListIds)
    for (let row of data) {
      if (!row.task_list_id) continue
      dbTaskListIdsSet.add(row.task_list_id)
    }
    const updateTaskListQueries = Array.from(dbTaskListIdsSet).map(id => {
      return (this.knex<DB.task_list>('task_list')
        .where('id', id)
        .update({
          updated_at: (new Date()).toISOString(),
        }))
    })

    /* update tasks */
    const updateQueries = data.map((row, idx) => {
      const updateBody: Partial<UpdateTaskData> = row
      delete updateBody.id
      return (this.knex<DB.task>('task')
        .where('id', dbTaskIds[idx])
        .update({
          ...updateBody,
          updated_at: (new Date()).toISOString(),
        }))
        .timeout(1000, { cancel: true })
    })
    await Promise.all([...updateQueries, ...updateTaskListQueries])
  }

  // batch create tasks service for a single task list
  createTasks = async (userId: number, taskListId: number, tasks: API.PostTasksBody['tasks']) => {
    /* check of task list exists */
    let taskListRes: Pick<DB.task_list, "id" | "owner_id"> | undefined
    taskListRes = await this.knex<DB.task_list>('task_list')
      .select('id', 'owner_id')
      .where('id', taskListId)
      .first()
    if (!taskListRes) throw new BadRequestError(`Task list ${taskListId} does not exist.`)
    
    /* check if user is owner of task list */
    const dbTaskListOwnerId = taskListRes.owner_id
    if (userId !== dbTaskListOwnerId) {
      throw new UnauthorizedError(`Unauthorized access on task list ${taskListId}`)
    }
    
    /* database operation */
    const dbTaskListId = taskListRes.id
    const currentISOString = (new Date()).toISOString()
    const createQueries: Knex.QueryBuilder<DB.task, number[]>[] = tasks.map(row => {
      return this.knex<DB.task>('task')
        .insert({
          owner_id: userId,
          task_list_id: dbTaskListId,
          task_name: row.taskName,
          description: row.description,
          deadline: row.deadline,
          completed: false,
          created_at: currentISOString,
          updated_at: currentISOString,
        })
    })
    await Promise.all(createQueries)
  }
}