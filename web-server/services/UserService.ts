import { Knex } from 'knex'
import { hashPassword, comparePassword } from '../helpers/bcrypt'
import { DB, JWTPayload } from 'shared'
import jwtSimple from 'jwt-simple'
import { jwtConfig } from '../helpers/jwt'
import { BadRequestError, DuplicationError } from '../helpers/error'

export default class UserService {
  constructor(private knex: Knex) { }

  validateUser = async (userId: number) => {
    let userRes: Pick<DB.user, "id" | "email"> | undefined
    userRes = await this.knex<DB.user>('user')
      .select('id', 'email')
      .where('id', userId)
      .first()
    if (!userRes) throw new BadRequestError(`User ${userId} does not exist.`)
    const dbUserId = userRes.id
    const dbEmail = userRes.email
    return [dbUserId, dbEmail] as const
  }

  createUser = async (email: string, password: string) => {
    const isExisting = await this.knex<DB.user>('user')
      .select('*')
      .where({
        email
      })
      .first()
    if (isExisting) throw new DuplicationError(`User with email address ${email} already exists.`)

    const hash_password = await hashPassword(password)
    const res = await this.knex<DB.user>('user')
      .insert({
        email,
        hash_password,
        created_at: (new Date()).toISOString(),
        updated_at: (new Date()).toISOString(),
      }).returning(['id'])
    if (!res) throw new Error('Unknown error during user creation.')

    let payload: JWTPayload = {
      id: res[0].id,
      email,
    }
    let token = jwtSimple.encode(payload, jwtConfig.SECRET)
    return token
  }

  loginWithPassword = async (email: string, password: string) => {
    const info = await this.knex<DB.user>('user')
      .select('id', 'hash_password')
      .where({ email })
      .first()

    if (!info) throw new BadRequestError('Incorrect email address.')
    if (!(await comparePassword(password, info.hash_password))) {
      throw new BadRequestError('Incorrect email address or password')
    }

    let payload: JWTPayload = {
      id: info.id,
      email: email,
    }
    let token = jwtSimple.encode(payload, jwtConfig.SECRET)
    return token;
  }
}