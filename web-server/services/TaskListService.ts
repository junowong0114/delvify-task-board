import { Knex } from 'knex'
import { API, DB } from 'shared'
import { BadRequestError } from '../helpers/error'
import { taskService } from '../app'
import { UpdateTaskData, UpdateTaskListData } from '../helpers/types'

export default class TaskListService {
  constructor(private knex: Knex) { }

  updateTaskList = async (taskListId: number, info: Omit<UpdateTaskListData, 'id'>, tasks?: UpdateTaskData[]) => {
    /* validate task list id */
    const validateTaskListRes = await this.knex<DB.task_list>('task_list')
      .select('id')
      .where('id', taskListId)
      .first()
    if (!validateTaskListRes) throw new BadRequestError(`Task list ${taskListId} does not exist.`)
    const dbTaskListId = validateTaskListRes.id

    /* update the task list */
    await this.knex<DB.task_list>('task_list')
      .where('id', dbTaskListId)
      .update({
        ...info,
        updated_at: (new Date()).toISOString()
      })

    /* update the tasks */
    if (!tasks) return
    await taskService.updateTasks(tasks)
  }

  // batch create task lists service 
  createTaskLists = async (userId: number, taskLists: API.PostTaskListsBody['taskLists']) => {
    /* create task lists */
    const currentISOString = (new Date()).toISOString() // construct create lists queries
    const createListsQueries = taskLists.map(row => {
      return this.knex<DB.task_list>('task_list')
        .insert({
          owner_id: userId,
          task_list_name: row.taskListName,
          created_at: currentISOString,
          updated_at: currentISOString,
        })
        .returning(['id'])
    })
    let taskListIds: (Pick<DB.task_list, "id"> | undefined)[]
    taskListIds = (await Promise.all(createListsQueries))[0] // retrieve task list ids for create tasks operation
    if (taskListIds.some(row => !row)) throw new Error('Unknown error during task list creation')

    /* create tasks */
    const createTasksQueries = taskLists.map((row, idx) => { // construct create tasks queries
      return taskService.createTasks(userId, (taskListIds[idx] as { id: number }).id, row.tasks)
    })
    await Promise.all(createTasksQueries) // database operation
  }

  getTaskList = async (taskListId: number) => {
    /* database operation */
    let rows: any
    rows = (await this.knex.raw(`
      WITH tmp AS (
        SELECT task_list.id as task_list_id,
          jsonb_build_object(
            'taskId', task.id,
            'taskName', task.task_name,
            'description', task.description,
            'deadline', task.deadline,
            'completed', task.completed
          ) as tasks
        FROM task
        RIGHT OUTER JOIN task_list ON task.task_list_id = task_list.id
        ORDER BY task_list.id, task.id, task.created_at ASC
      )
      SELECT task_list.id as task_list_id,
        task_list.owner_id,
        task_list.task_list_name,
        jsonb_agg(tmp.tasks) as tasks
      FROM tmp
        RIGHT OUTER JOIN task_list ON task_list.id = tmp.task_list_id
      WHERE task_list.id = ?
      GROUP BY task_list.id, task_list.owner_id, task_list.task_list_name
      ORDER BY task_list.id ASC`,
      [taskListId])).rows

    /* validate result */
    if (!rows.length) throw new BadRequestError(`Task list with id ${taskListId} does not exist.`)

    return rows[0]
  }

  getTaskLists = async (userId: number) => {
    /* database operation */
    let rows: any

    rows = (await this.knex.raw(`
      WITH tmp AS (
        SELECT task_list.id as task_list_id,
          jsonb_build_object(
            'taskId', task.id,
            'taskName', task.task_name,
            'description', task.description,
            'deadline', task.deadline,
            'completed', task.completed
          ) as tasks
        FROM task
        RIGHT OUTER JOIN task_list ON task.task_list_id = task_list.id
        ORDER BY task_list.id, task.id, task.created_at ASC
      )
      SELECT task_list.id as task_list_id,
        task_list.owner_id,
        task_list.task_list_name,
        jsonb_agg(tmp.tasks) as tasks
      FROM tmp
        RIGHT OUTER JOIN task_list ON task_list.id = tmp.task_list_id
      WHERE owner_id = ?
      GROUP BY task_list.id, task_list.owner_id, task_list.task_list_name
      ORDER BY task_list.id ASC`,
      [userId])).rows

    return rows
  }

  // batch delete task lists
  deleteTaskLists = async (taskListIds: number[]) => {
    const deleteQueries: Knex.QueryBuilder<DB.task, number>[] = []

    // both task lists and tasks inside the lists are to be deleted
    const [deleteTaskQueries, deleteTaskListQueries] = taskListIds
      .reduce((queries, id) => {
        const deleteTaskQuery = queries[0].orWhere('task_list_id', id)
        const deleteTaskListQuery = queries[1].orWhere('id', id)

        return ([deleteTaskQuery, deleteTaskListQuery])
      }, [this.knex<DB.task>('task'), this.knex<DB.task>('task_list')])

    deleteQueries.push(deleteTaskQueries.del(), deleteTaskListQueries.del())

    await Promise.all(deleteQueries)
  }
}