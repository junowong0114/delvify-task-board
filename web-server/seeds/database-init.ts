import { Knex } from 'knex'
import { DB } from 'shared'
import { hashPassword } from '../helpers/bcrypt'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('task').del()
    await knex('task_list').del()
    await knex('user').del()

    const todayISOstring = (new Date()).toISOString()

    const userIds = await knex<DB.user>('user').insert([
      {
        email: 'admin@gmail.com',
        hash_password: await hashPassword('admin'),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        email: 'user@gmail.com',
        hash_password: await hashPassword('user'),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
    ]).returning(['id'])
    
    const taskListIds = await knex<DB.task_list>('task_list').insert([
      {
        owner_id: userIds[0].id,
        task_list_name: 'Buy grocery',
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        owner_id: userIds[0].id,
        task_list_name: 'Chores',
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        owner_id: userIds[0].id,
        task_list_name: 'Empty list',
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
    ]).returning(['id'])

    await knex<DB.task>('task').insert([
      {
        owner_id: userIds[0].id,
        task_list_id: taskListIds[0].id,
        task_name: 'Buy milk',
        description: 'Buy milk from Welcome.',
        completed: true,
        deadline: (new Date(2021, 10, 21, 17, 0, 0)).toISOString(),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        owner_id: userIds[0].id,
        task_list_id: taskListIds[0].id,
        task_name: 'Buy toilet paper',
        description: 'Buy toilet paper from Welcome.',
        completed: false,
        deadline: (new Date(2021, 10, 21, 17, 0, 0)).toISOString(),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        owner_id: userIds[0].id,
        task_list_id: taskListIds[0].id,
        task_name: 'Buy ice cream',
        description: 'Buy ice cream from Welcome.',
        completed: true,
        deadline: (new Date(2021, 10, 21, 17, 0, 0)).toISOString(),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        owner_id: userIds[0].id,
        task_list_id: taskListIds[1].id,
        task_name: 'Clean kitchen',
        description: 'Clean sink and range hood.',
        completed: true,
        deadline: (new Date(2021, 10, 30, 17, 0, 0)).toISOString(),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
      {
        owner_id: userIds[0].id,
        task_list_id: taskListIds[1].id,
        task_name: 'Wash clothes',
        description: '',
        completed: false,
        deadline: (new Date(2021, 10, 30, 17, 0, 0)).toISOString(),
        created_at: todayISOstring,
        updated_at: (new Date(2050, 0, 1, 0, 0, 0)).toISOString(),
      },
      {
        owner_id: userIds[0].id,
        task_list_id: taskListIds[1].id,
        task_name: 'Clean bedroom',
        description: 'Buy a new vacuum to clean the room.',
        completed: true,
        deadline: (new Date(2021, 10, 30, 17, 0, 0)).toISOString(),
        created_at: todayISOstring,
        updated_at: todayISOstring,
      },
    ]).returning(['id'])

    // await knex<DB.task_list_task>('task_list_task').insert([
    //   {
    //     task_list_id: taskListIds[0].id,
    //     task_id: taskIds[0].id
    //   },
    //   {
    //     task_list_id: taskListIds[0].id,
    //     task_id: taskIds[1].id
    //   },
    //   {
    //     task_list_id: taskListIds[0].id,
    //     task_id: taskIds[2].id
    //   },
    //   {
    //     task_list_id: taskListIds[1].id,
    //     task_id: taskIds[3].id
    //   },
    //   {
    //     task_list_id: taskListIds[1].id,
    //     task_id: taskIds[4].id
    //   },
    //   {
    //     task_list_id: taskListIds[1].id,
    //     task_id: taskIds[5].id
    //   },
    // ])
};
