import { Knex } from 'knex'
import { DB } from 'shared'
import { hashPassword } from '../helpers/bcrypt'

export async function seed(knex: Knex): Promise<void> {
  await knex('task').del()
  await knex('task_list').del()
  await knex('user').del()

  const todayISOstring = (new Date()).toISOString()

  await knex<DB.user>('user').insert([
    {
      email: 'user1@gmail.com',
      hash_password: await hashPassword('user'),
      created_at: todayISOstring,
      updated_at: todayISOstring,
    },
    {
      email: 'user2@gmail.com',
      hash_password: await hashPassword('user'),
      created_at: todayISOstring,
      updated_at: todayISOstring,
    },
  ])
};
