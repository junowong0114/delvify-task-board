import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  const schemaBuilders: Knex.SchemaBuilder[] = []

  schemaBuilders.push(knex.schema.createTable('user', table => {
    table.increments()
    table.string('email').notNullable
    table.string('hash_password').notNullable
    table.timestamp('created_at', { useTz: false }).notNullable
    table.timestamp('updated_at', { useTz: false }).notNullable
  }))
  
  schemaBuilders.push(knex.schema.createTable('task_list', table => {
    table.increments()
    table.integer('owner_id')
    table.foreign('owner_id').references('id').inTable('user')
    table.string('task_list_name')
    table.timestamp('created_at', { useTz: false }).notNullable
    table.timestamp('updated_at', { useTz: false }).notNullable
  }))

  schemaBuilders.push(knex.schema.createTable('task', table => {
    table.increments()
    table.integer('task_list_id')
    table.foreign('task_list_id').references('id').inTable('task_list')
    table.integer('owner_id')
    table.foreign('owner_id').references('id').inTable('user')
    table.string('task_name').notNullable
    table.text('description')
    table.boolean('completed').notNullable
    table.timestamp('deadline', { useTz: false }).notNullable
    table.timestamp('created_at', { useTz: false }).notNullable
    table.timestamp('updated_at', { useTz: false }).notNullable
  }))


  // schemaBuilders.push(knex.schema.createTable('task_list_task', table => {
  //   table.integer('task_list_id')
  //   table.foreign('task_list_id').references('id').inTable('task_list')
  //   table.integer('task_id')
  //   table.foreign('task_id').references('id').inTable('task')
  // }))

  await Promise.all(schemaBuilders)
}


export async function down(knex: Knex): Promise<void> {
  const tableDestroyers = []

  tableDestroyers.push(knex.schema.dropTableIfExists('task'))
  tableDestroyers.push(knex.schema.dropTableIfExists('task_list'))
  tableDestroyers.push(knex.schema.dropTableIfExists('user'))

  await Promise.all(tableDestroyers)
}

