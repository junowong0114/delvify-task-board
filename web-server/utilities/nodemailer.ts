// a mock nodemailer transporter class

type CreateTransportOptions = {
  service: string,
  auth: {
    user: string,
    pass: string,
  }
}

type MailOptions = {
  from: string,
  to: string,
  subject: string,
  text: string
}

export class NodeMailer {
  constructor() { }

  createTransport = (options: CreateTransportOptions) => {
    return new Transporter(options)
  }
}

type SendMailRes = {
  msg: string,
  from: string,
  to: string,
  subject: string,
  text: string,
  service: string,
  auth: CreateTransportOptions['auth']
}

export class Transporter {
  constructor(private options: CreateTransportOptions) { }

  sendMail = (options: MailOptions) => {
    return new Promise<SendMailRes>((resolve, reject) => {
      setTimeout(() => {
        console.log('')
        console.log(`Sent email to ${options.to}`)
        console.log(`From: ${options.from}`)
        console.log(`To: ${options.to}`)
        console.log(`Subject: ${options.subject}`)
        console.log(`Content: ${options.text}`)
        console.log('---------------------------------')
        console.log(`Service: ${this.options.service}`)
        console.log(`Credentials:`)
        console.log(` User: ${this.options.auth.user}`)
        console.log(` Password: ${this.options.auth.pass}`)
        resolve({
          msg: `Message sent to <${options.to}>`,
          from: options.from,
          to: options.to,
          subject: options.subject,
          text: options.text,
          service: this.options.service,
          auth: this.options.auth
        })
      }, 1000)
    })
  }
}

const nodemailer = new NodeMailer

export default nodemailer