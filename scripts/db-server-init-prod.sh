#!/bin/bash
set -e
set -o pipefail
set -x

echo "# node environment
NODE_ENV=development

# PORT number for web server
PORT=8800

# others
JWT_SECRET=delvifycodetest

# pg database
DEV_DB_NAME=task_board
DEV_DB_USER=postgres
DEV_DB_PASSWORD=postgres

TEST_DB_NAME=task_board_test
TEST_DB_USER=postgres
TEST_DB_PASSWORD=postgres

POSTGRES_DB=task_board
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_HOST='localhost'" > ./web-server/.env

docker-compose up -d
