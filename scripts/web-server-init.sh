#!/bin/bash
set -e
set -o pipefail
set -x

./scripts/local/update-shared.sh

cd ./web-server
npm install
npm run db-prepare && npm run dev
