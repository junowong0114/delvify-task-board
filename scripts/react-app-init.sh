#!/bin/bash
set -e
set -o pipefail
set -x

cd ./react-app
yarn install
echo "NODE_ENV=development
DISABLE_ESLINT_PLUGIN=true" > .env
yarn start