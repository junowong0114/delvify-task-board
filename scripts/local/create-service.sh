read -p "service name: " name
read -p "PORT: " PORT

mkdir -p $name
cd $name

npm init --yes
npm install --save @types/cors
npm install --save @types/express
npm install --save cors
npm install --save dotenv
npm install --save express
npm install --save listening-on
npm install --save ts-node

echo "FROM node:slim

WORKDIR /srv/taskboard-user-service-server

RUN npm init --yes
RUN npm install --save ts-node
RUN npm install --save express
RUN npm install --save @types/express
RUN npm install --save cors
RUN npm install --save @types/cors
RUN npm install knex

COPY package.json .
RUN npm install --production

CMD npx knex migrate:latest \\
  npx knex migrate seed:run \\
  node index.js" > Dockerfile

echo "require('ts-node/register')
require('./app')" > index.js

echo "" > knexfile.ts

echo "" > .dockerignore

echo "PORT=$PORT" > .env

echo "import express, { Request, Response } from 'express'
import cors from 'cors'
import dotenv from 'dotenv'

dotenv.config()

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.get('/', (req: Request, res: Response) => {
  res.end('welcome')
})

const PORT = process.env.PORT
app.listen(PORT, () => {
  console.log(\`Listening on PORT \${PORT}\`)
})" > app.ts