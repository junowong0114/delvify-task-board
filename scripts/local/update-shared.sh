#!/bin/bash
set -e
set -o pipefail

cd shared
npm install
npm run build

cd ../web-server
rm -rf lib
mkdir lib
cp -a ../shared ./lib/shared
npm install

cd ../react-app
yarn add ../shared

cd ..