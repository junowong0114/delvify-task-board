#!/bin/bash
set -e
set -o pipefail
set -x

cd shared
npm install
npm run build

cd ../web-server
rm -rf lib
mkdir lib
cp -r ../shared ./lib/shared
npm install
docker build -t taskboard-web-image .

cd ../