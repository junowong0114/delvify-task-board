#!/bin/bash
set -e
set -o pipefail
set -x

cd shared
npm install
npm run build

cd ../web-server
rm -rf lib
mkdir lib
cp -a ../shared ./lib/shared
npm install
npm run db-prepare && forever start ./web-server/index.js
