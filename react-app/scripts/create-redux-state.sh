read -p "State name: " name

capName="$(echo ${name^})"
state="$capName""State"
action="$capName""Action"
reducer="$name""Reducer"

mkdir -p src/redux/$name
cd src/redux/$name

# state.ts
echo "export type $state = {

}

export const initialState: $state = {

}" > state.ts

# action.ts
echo "export function init$state() {
  return {
    type: 'init$state' as const,
  }
}

export type $action = ReturnType<typeof init$state>" > action.ts

# reducer.ts
echo "import { $state, initialState } from './state'
import { $action } from './action'

export const $reducer = (
  state: $state = initialState,
  action: $action
): $state => {
  switch(action.type) {
    default:
      return state
  }
}" > reducer.ts

echo "import { RootDispatch } from '../dispatch'
import { init$state } from './action'

export function init"$state"Thunk() {
  return async (dispatch: RootDispatch) => {
    dispatch(init$state())
  }
}" > thunk.ts