read -p "Component name: " name
read -p "Directory: " directory

mkdir -p $directory
cd $directory

# tsx file
echo "// style
import './$name.scss'

const $name: React.FC = () => {
  return (
    <div className="\""$name"\"">

    </div>
  )
}

export default $name" > $name.tsx

# scss file
echo ".$name {

}" > $name.scss