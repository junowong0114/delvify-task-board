import { DTO } from 'shared'
import * as assert from './assert'
import { Task, TaskList } from './types'

export function formatTaskList(taskList: DTO.TaskList): TaskList {
  return {
    ...taskList,
    tasks: taskList.tasks.map(row => {
      return formatTask(row, taskList.taskListId)
    })
  }
}

export function formatTask(task: DTO.Task, taskListId: number): Task {
  const deadlineDate = new Date(task.deadline)

  if (!assert.isValidDate(deadlineDate)) {
    throw new Error('Corruption in input data, deadline cannot be converted to Date object')
  }

  return {
    ...task,
    taskListId,
    deadline: deadlineDate,
  }
}