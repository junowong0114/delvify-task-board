import { isValidDate } from "./assert";

describe('isValidDate', () => {
  it('should return true for Date object', () => {
    const testDate = new Date()
    expect(isValidDate(testDate)).toBe(true)
  })

  it('should return false for numbers', () => {
    const testNum: number = 1
    expect(isValidDate(testNum)).toBe(false)
  })

  it('should return false for ISO date string', () => {
    const ISOstring = (new Date()).toISOString()
    expect(isValidDate(ISOstring)).toBe(false)
  })

  it('should return false for UTC date string', () => {
    const UTCstring = (new Date()).toUTCString()
    expect(isValidDate(UTCstring)).toBe(false)
  })
})