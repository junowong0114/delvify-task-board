export function getAPIServer() {
  if (process.env.NODE_ENV === 'development') {
    return 'http://localhost:8080'
  }

  const { REACT_APP_API_SERVER } = process.env
  if (!REACT_APP_API_SERVER) {
    console.error("missing REACT_APP_API_SERVER in env")
    throw new Error("missing REACT_APP_API_SERVER in env")
  }
  return REACT_APP_API_SERVER
}