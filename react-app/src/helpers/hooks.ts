import { useLayoutEffect, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { API, errorToString } from 'shared';
import { loginFailed } from '../redux/auth/action';
import { checkTokenThunk } from '../redux/auth/thunk';
import { RootState } from '../redux/state';
import { FetchData } from '../helpers/types'

function generateOptions(method: 'GET' | 'DELETE' | 'POST' | 'PUT', token: string, options?: RequestInit, body?: Object) {
  let combinedOptions: RequestInit
  if (options) {
    combinedOptions = {
      ...options,
      headers: {
        ...options.headers,
        "Authorization": `Bearer ${token}`
      },
    }
  } else {
    combinedOptions = {
      headers: {
        "Authorization": `Bearer ${token}`
      },
    }
  }

  if (method !== 'GET') {
    combinedOptions = {
      ...combinedOptions,
      method,
      headers: {
        ...combinedOptions.headers,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body)
    }
  }

  return combinedOptions
}

function generateURL(origin: string, params?: string[], searchParams?: URLSearchParams) {
  let output = origin
  if (params) {
    output = output + params.reduce((params, current) => {
      return params + `/${current}`
    }, '')
  }
  if (searchParams) {
    output = output + searchParams.toString()
  }
  return output
}

export function useDel(url: string, options?: RequestInit) {
  const [loading, setLoading] = useState<boolean>(false)
  const [res, setRes] = useState<API.DelResponse>()

  function fetchNow<D>(token: string, body: D) {
    setLoading(true)
    const combinedOptions = generateOptions('DELETE', token, options, body)

    fetch(url, combinedOptions)
      .then((res: Response) => {
        if (res.status !== 200) throw new Error(`Server error with status code ${res.status}`)
        return res.json() as Promise<API.DelResponse>
      })
      .then((res) => {
        setLoading(false)
        setRes(res)
      })
      .catch(e => {
        setLoading(false)
        setRes({ success: false, error: errorToString(e) })
      })
  }

  return [fetchNow, res, loading] as const
}

export function useGet<D>(url: string, options?: RequestInit) {
  const [loading, setLoading] = useState<boolean>(false)
  const [data, setData] = useState<FetchData<D>>()

  useEffect(() => {
    return () => {
      setData(undefined)
    }
  }, [])

  function fetchNow(token: string, params?: string[], searchParams?: URLSearchParams) {
    setLoading(true)
    const combinedOptions = generateOptions('GET', token, options)
    const combinedUrl = generateURL(url, params, searchParams)
    
      fetch(combinedUrl, combinedOptions)
        .then((res: Response) => {
          if (res.status !== 200) throw new Error(`Server error with status code ${res.status}`)
          return res.json() as Promise<API.GetResponse<D>>
        })
        .then((res) => {
          setLoading(false)
          if ('result' in res) {
            setData({ data: res.result })
          } else {
            setData({ error: res.error as string })
          }
        })
        .catch(e => {
          setLoading(false)
          setData({ error: errorToString(e) })
        })
  }

  return [fetchNow, data, loading] as const
}

export function useWindowSize() {
  const [size, setSize] = useState([0, 0]);

  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return size;
}

export function useGuard() {
  const token = useSelector((state: RootState) => state.auth.token)
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(() => {
    if (!token) {
      dispatch(checkTokenThunk())
      if (!localStorage.getItem('taskBoardToken')) {
        dispatch(loginFailed('Please login in'))
        history.push('/tabs/login')
      }
    }
  }, [])

  return
}