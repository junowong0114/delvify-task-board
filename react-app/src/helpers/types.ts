import { DTO } from 'shared'

export type Task = Omit<DTO.Task, 'deadline'> & {
  taskListId: number,
  deadline: Date,
}

export type TaskList = Omit<DTO.TaskList, 'tasks'> & {
  tasks: Task[]
}

export type Modify<T, R> = Omit<T, keyof R> & R

export type TaskBoardTaskList = (TaskList & { isSelected: boolean })

// for useGet hook
export type FetchData<D> = { data: D } | { error: string }