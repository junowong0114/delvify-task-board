import { formatTaskList } from './TaskListFormat'
import * as assert from './assert'
import { DTO } from 'shared'
import { Modify } from './types'

const isValidDateSpy = jest.spyOn(assert, 'isValidDate')

describe('formatTaskList', () => {
  let validDTOTaskList: DTO.TaskList
  let invalidDTOTaskList: Modify<DTO.TaskList, {
    tasks: Modify<DTO.Task, {
      deadline: any
    }>[]
  }>

  beforeAll(() => {
    validDTOTaskList = {
      taskListId: 1,
      ownerId: 1,
      taskListName: 'To do list',
      tasks: [{
        taskId: 1,
        taskName: 'Buy milk',
        description: 'Go to Welcome to buy milk',
        deadline: (new Date()).toISOString(),
        completed: false,
      }]
    }

    invalidDTOTaskList = {
      taskListId: 1,
      ownerId: 1,
      taskListName: 'To do list',
      tasks: [{
        taskId: 1,
        taskName: 'Buy milk',
        description: 'Go to Welcome to buy milk',
        deadline: '123',
        completed: false,
      },
      {
        taskId: 1,
        taskName: 'Buy milk',
        description: 'Go to Welcome to buy milk',
        deadline: (new Date()).toISOString(),
        completed: false,
      }]
    }
  })
  
  it('should convert all ISO strings in task list to Date object', () => {
    isValidDateSpy.mockReturnValue(true)
    const tasks = formatTaskList(validDTOTaskList).tasks
    
    expect(isValidDateSpy).toBeCalledTimes(1)
    expect(tasks.every(task => task.deadline instanceof Date)).toBe(true)

    isValidDateSpy.mockReset()
  })
  
  it('should throw error if isValidDate return false', () => {
    isValidDateSpy.mockReturnValue(false)
    expect(() => formatTaskList(validDTOTaskList)).toThrow()

    isValidDateSpy.mockReset()
  })

  it('should throw error if some of the deadline cannot be converted to Date object', () => {
    expect(() => formatTaskList(invalidDTOTaskList)).toThrow()
  })
})
