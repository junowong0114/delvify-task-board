export function roundTime(date: Date, resolutionInMin: number = 30) {
  const coeff = 1000 * 60 * resolutionInMin
  return (new Date(Math.round(date.getTime() / coeff) * coeff))
}