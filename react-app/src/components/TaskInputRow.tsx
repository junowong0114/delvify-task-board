// components
import { Form } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import CustomButton from './CustomButton'
import { FaTrash } from "react-icons/fa"

// hooks and helpers
import { useEffect, useState } from 'react'
import { roundTime } from '../helpers/format'

// style
import './TaskInputRow.scss'
import "react-datepicker/dist/react-datepicker.css";

//types
import { Task } from '../helpers/types'

type Props = {
  data: Task | Omit<Task, 'completed'>,
  onChange: (updatedData: Task) => void,
  onDelete: () => void,
  onSelect: (wasSelected: boolean) => void,
  isSelected: boolean,
}

const TaskInputRow: React.FC<Props> = (props) => {
  const [taskName, setTaskName] = useState({ value: '' })
  const [deadline, setDeadline] = useState(roundTime(new Date()))
  const [description, setDescription] = useState({ value: '' })

  useEffect(() => {
    setTaskName({ value: props.data.taskName })
    setDeadline(props.data.deadline)
    setDescription({ value: props.data.description })
  }, [])

  useEffect(() => {
    const submitData: Task = {
      taskId: props.data.taskId,
      taskName: taskName.value,
      taskListId: props.data.taskListId,
      description: description.value,
      completed: 'completed' in props.data? props.data.completed : false,
      deadline
    }
    props.onChange(submitData)
  }, [taskName, description, deadline])

  function onDateChange(date: Date | [Date | null, Date | null] | null) {
    if (date instanceof Date) {
      // TODO: add logic to prevent user setting deadline < new Date()
      setDeadline(date)
    }
  }

  return (
    <div className="TaskInputRow">
      <Form.Check 
      type="checkbox" 
      className="check-box"
      checked={props.isSelected}
      onChange={() => props.onSelect(props.isSelected)} />
      <div className="input-container">
        <div className="title">
          <label>Task name:</label>
          <Form.Control
            type="text"
            placeholder="Task name"
            value={taskName.value}
            onChange={(e) => setTaskName({ value: e.target.value })} />
        </div>
        <div className="deadline">
          <label>Deadline: </label>
          <DatePicker
            selected={deadline}
            onChange={onDateChange}
            showTimeSelect
            dateFormat="Pp"
            className="deadline-picker" />
        </div>
        <div className="description">
          <label>Description:</label>
          <Form.Control
            as="textarea"
            placeholder="Description"
            style={{ height: '21px' }}
            value={description.value}
            onChange={(e) => setDescription({ value: e.target.value })} />
        </div>
      </div>
      <div className="trash-btn-container">
        <CustomButton
          icon={<FaTrash />}
          onClick={props.onDelete}
          type="secondary"
        />
      </div>
    </div>
  )
}

export default TaskInputRow
