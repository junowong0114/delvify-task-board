// components
import {
  ProSidebar,
  SidebarHeader,
  SidebarContent,
  Menu,
  MenuItem,
  SidebarFooter,
} from 'react-pro-sidebar'
import { FaGem } from 'react-icons/fa'
import {IoLogOut} from 'react-icons/io5'

// redux and hooks
import { useDispatch } from 'react-redux'
import { logoutThunk } from '../redux/auth/thunk'
import { Link, useHistory } from 'react-router-dom'

// style
import './SideBar.scss'

type Props = {
  toggled: boolean,
  onToggle: () => void,
}

const SideBar: React.FC<Props> = (props) => {
  const history = useHistory()
  const dispatch = useDispatch()
  const path = history.location.pathname

  function onLogoutBtnClick() {
    dispatch(logoutThunk())
    history.push('/tabs/login')
  }

  return (
    <ProSidebar
      breakPoint="lg"
      toggled={props.toggled}
      onToggle={() => props.onToggle()}
    >
      <SidebarHeader>
        <Link to='/'>
          <h1>
            <span>Task</span>
            <span>Board</span>
          </h1>
        </Link>
      </SidebarHeader>

      <SidebarContent>
        <Menu iconShape="circle">
          <MenuItem icon={<FaGem />} active={path === '/tabs/dash_board'}>
            Dashboard
            <Link to='/'></Link>
          </MenuItem>
        </Menu>
        <Menu iconShape="circle">
          <MenuItem icon={<IoLogOut />} onClick={onLogoutBtnClick}>
            Logout
          </MenuItem>
        </Menu>
      </SidebarContent>

      <SidebarFooter>
        <p>v0.1.0</p>
      </SidebarFooter>
    </ProSidebar>
  )
}

export default SideBar
