// components
import CustomButton from './CustomButton'
import { IoChevronBack } from 'react-icons/io5' 

// hooks and helpers
import { useHistory } from 'react-router'

type Props = {
  onClick?: () => void
}

const BackButton: React.FC<Props> = (props) => {
  const history = useHistory()
  const onClick = props.onClick? props.onClick : () => history.goBack()

  return (
    <CustomButton
      icon={<IoChevronBack />}
      onClick={onClick}
      btnSize={45} />
  )
}

export default BackButton
