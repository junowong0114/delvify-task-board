import React from 'react'

type CustomToggleProps = {
  children?: React.ReactNode,
  onClick: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {};
}

const CustomToggle = React.forwardRef(
  (props: CustomToggleProps, ref: React.Ref<HTMLAnchorElement>) => (
    <a
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        props.onClick(e);
      }}
    >
      {props.children}
      &#x25bc;
    </a>
  ));

export default CustomToggle