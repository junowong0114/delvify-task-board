// style
import styles from './CustomButton.module.scss'

type Props = {
  icon?: JSX.Element,
  onClick: () => void,
  text?: string,
  hidden?: boolean,
  btnSize?: number,
  color?: string,
  type?: 'primary' | 'secondary' | 'secondaryLight'
  ref?: any
}

const CustomButton: React.FC<Props> = (props) => {
  function onClick(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault()
    props.onClick()
  }
  function getClassName() {
    const classNames = [styles.CustomButton]
    if (!props.text) classNames.push(styles.onlyIcon)
    switch(props.type) {
      case 'primary':
        classNames.push(styles.primaryBtn)
        break

      case 'secondary':
        classNames.push(styles.secondaryBtn)
        break

      case 'secondaryLight':
        classNames.push(styles.secondaryLightBtn)
        break

      default:
        classNames.push(styles.primaryBtn)
        break
    }

    return classNames.join(' ')
  }

  return (
    <button
      className={getClassName()}
      onClick={e => onClick(e)}
      hidden={props.hidden}
      ref={props.ref}
      style={{
        height: props.btnSize,
        width: props.btnSize,
      }} >
      {props.icon}{props.text}
    </button>
  )
}

export default CustomButton
