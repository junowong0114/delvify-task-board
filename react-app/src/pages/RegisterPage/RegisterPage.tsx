// components
import { Button, Container, Form } from 'react-bootstrap'

// hooks and helpers
import { useHistory } from 'react-router'
import { useEffect, useState } from 'react'

// redux
import { loginWithPassWordThunk, loginWithTokenThunk } from '../../redux/auth/thunk'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/state'

// style
import './RegisterPage.scss'
import { Link } from 'react-router-dom'
import { getAPIServer } from '../../helpers/api'

// types
import { API, errorToString } from 'shared'

const LoginPage: React.FC = () => {
  const [email, setEmail] = useState({ value: '' })
  const [password, setPassword] = useState({ value: '' })
  const [confirmPassword, setConfirmPassword] = useState({ value: '' })
  const [error, setError] = useState<string>()
  const history = useHistory()
  const dispatch = useDispatch()
  const [token, loginError] = useSelector((state: RootState) => {
    return [state.auth.token, state.auth.error]
  })

  // redirect to front page if token is found
  useEffect(() => {
    if (token) history.push('/dash_board')
  }, [token])

  // show error if login failed
  useEffect(() => {
    if (!loginError) return
    setError(loginError)
  }, [loginError])

  // validate password and confirm password on the fly
  function validatePassword() {
    if (confirmPassword.value.length === 0) return
    if (confirmPassword.value === password.value) {
      setError('')
      return
    } 
    setError('Password and confirm password does not match.')
  }

  function onSignUpBtnClick(e: any) {
    e.preventDefault()
    if (confirmPassword.value !== password.value) return setError('Password and confirm password does not match.')
    if (!email.value.length) return setError('Email address cannot be empty.')
    if (!password.value.length) return setError('Password cannot be empty.')

    const origin = getAPIServer()
    const requestBody: API.PostRegisterBody = {
      email: email.value,
      password: password.value,
    }
    fetch(`${origin}/register`, {
      method: "POST",
      headers: {
        "Content-Type": 'application/json'
      },
      body: JSON.stringify(requestBody)
    })
    .then(res => {
      return res.json() as Promise<API.PostRegisterResponse>
    })
    .then(res => {
      if ('error' in res) throw new Error(res.error)
      dispatch(loginWithTokenThunk(res.token))
      history.push('/tabs/dash_board')
    })
    .catch(e => {
      setError(errorToString(e))
    })
  }

  return (
    <div className="LoginPage">
      <Container fluid>
        <h2>Register</h2>
        <main className="main-hero">
          <Form className="login-form">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Email"
                autoComplete="off"
                value={email.value}
                onChange={e => setEmail({ value: e.target.value })} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control
                type="password"
                placeholder="Password"
                autoComplete="off"
                value={password.value}
                onChange={e => setPassword({ value: e.target.value })} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
              <Form.Control
                type="password"
                placeholder="Confirm password"
                autoComplete="off"
                value={confirmPassword.value}
                onChange={e => setConfirmPassword({ value: e.target.value })}
                onBlur={validatePassword} />
            </Form.Group>

            <Button variant="primary" type="submit" onClick={e => onSignUpBtnClick(e)}>
              Create my account
            </Button>
          </Form>

          <div className="sign-up">
            Already have an account? <Link to="/tabs/login">Login</Link>
          </div>

          <div className="error-msg">
            {error}
          </div>
        </main>
      </Container>
    </div>
  )
}

export default LoginPage
