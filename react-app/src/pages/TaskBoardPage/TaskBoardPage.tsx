// components
import { InputGroup } from "react-bootstrap"
import { Container } from "react-bootstrap"
import { FaPen, FaTrash } from "react-icons/fa"
import { ImExit } from "react-icons/im"
import { IoAddCircle } from "react-icons/io5"
import ListCard from "./components/ListCard"
import CustomButton from "../../components/CustomButton"

// redux
import { useSelector } from "react-redux"
import { RootState } from "../../redux/state"

// hooks and helper functions
import { useDel, useGet } from '../../helpers/hooks'
import { useEffect, useState } from "react"
import { formatTaskList } from "../../helpers/TaskListFormat"
import { getSelectedTaskListIds, setSingleCardSelected, isSomeSelected } from './helpers'
import { useStorageState } from 'react-use-storage-state'

// style
import "./TaskBoardPage.scss"

// types
import { API, DTO, errorToString } from 'shared'
import { TaskBoardTaskList } from '../../helpers/types'
import { getAPIServer } from "../../helpers/api"
import { useHistory } from "react-router"

const TaskBoardPage: React.FC = () => {
  // page display state
  const [isEditing, setIsEditing] = useState<boolean>(false)
  const [isSelecting, setIsSelecting] = useState<boolean>(false)

  // server/data related
  const origin = getAPIServer()
  const token = useSelector((state: RootState) => state.auth.token)
  const [fetchNow, rawData, loading] = useGet<DTO.TaskList[]>(`${origin}/task_lists`)
  const [deleteNow, delRes, deleting] = useDel(`${origin}/task_lists`)
  const [error, setError] = useState<string>('')
  const [taskLists, setTaskLists] = useStorageState<TaskBoardTaskList[]>('taskLists', [])
  const history = useHistory()

  // fetch task lists on load
  useEffect(() => {
    if (!token) return
    fetchNow(token)
  }, [token])

  // convert fetch result to useful data
  useEffect(() => {
    if (!rawData) return
    if ('error' in rawData) return setError(rawData.error)

    let data: Array<ReturnType<typeof formatTaskList> & { isSelected: boolean }>
    try {
      data = rawData.data.map(row => { return { ...formatTaskList(row), isSelected: false } })
      setTaskLists(data)
    } catch (e) {
      return setError(errorToString(e))
    }
  }, [rawData])

  // toggle isEditing and isSelecting according to num of cards selected
  useEffect(() => {
    if (isSomeSelected(taskLists)) {
      setIsEditing(true)
      setIsSelecting(true)
    } else {
      setIsSelecting(false)
    }
  }, [taskLists])

  // actions when delete success / failed
  useEffect(() => {
    if (!delRes) return
    if (deleting) return
    if ('error' in delRes) setError(delRes.error as string)
    fetchNow(token as string) 
  }, [delRes, deleting])

  // select/unselect all cards logic
  function onSelectAllChecked() {
    setTaskLists(taskLists.map(row => { return { ...row, isSelected: isSelecting ? false : true } }))
    setIsSelecting(!isSelecting)
  }

  // select/unselect single card logic
  function onSelectTaskCard(taskListId: number, action: 'select' | 'unselect') {
    const newSelectedValue = action === 'select'
    const newTaskLists = setSingleCardSelected(taskLists, taskListId, newSelectedValue)

    setTaskLists(newTaskLists)
  }

  // exit edit mode button pressed
  function onExitPressed() {
    setIsEditing(false)
    // unselect all cards
    setTaskLists(taskLists.map(row => { return { ...row, isSelected: false } }))
    setIsSelecting(false)
  }

  // batch delete task list logic
  function onDeleteMultipleTaskList() {
    const taskListIds = getSelectedTaskListIds(taskLists)
    deleteNow<API.DelTaskListsBody>(token as string, { taskListIds: Array.from(taskListIds) })
  }

  // delete single task list logic
  function onDeleteSingleTaskList(taskListId: number) {
    deleteNow<API.DelTaskListsBody>(token as string, { taskListIds: [taskListId] })
  }

  return (
    <div className="TaskBoardPage">
      <Container fluid>
        <h2>Dashboard</h2>
        <div className="action-bar">
          <div className="slot start">
            <InputGroup.Checkbox
              aria-label="Checkbox for select all"
              hidden={!isEditing}
              checked={isSelecting}
              onChange={() => onSelectAllChecked()} />
            <CustomButton
              icon={<FaTrash />}
              text='Delete'
              onClick={() => onDeleteMultipleTaskList()}
              type={'secondary'}
              hidden={!isEditing} />
          </div>
          <div className="slot end">
            <CustomButton
              icon={<IoAddCircle style={{ fontSize: '21px' }} />}
              text='Add a task list'
              onClick={() => { history.push(`/task_list/create`) }}
              type={'secondary'} />
            <CustomButton
              icon={<FaPen />}
              text='Edit'
              onClick={() => setIsEditing(true)}
              type={'secondary'}
              hidden={isEditing} />
            <CustomButton
              icon={<ImExit />}
              text='Exit edit mode'
              onClick={() => onExitPressed()}
              type={'secondary'}
              hidden={!isEditing} />
          </div>
        </div>

        <div className="error-text">{error}</div>

        <div className="list-card-container">
          {taskLists.map((row, idx) => {
            return (
              <ListCard
                key={row.taskListId}
                isEditing={isEditing}
                data={row}
                onSelect={(action: 'select' | 'unselect') => onSelectTaskCard(row.taskListId, action)}
                onDelete={() => onDeleteSingleTaskList(row.taskListId)}
                onError={(msg: string) => setError(msg)}
                onDeleteTask={() => fetchNow(token as string)} />
            )
          })}
        </div>
      </Container>
    </div>
  )
}

export default TaskBoardPage