import { TaskBoardTaskList } from "../../helpers/types"

export function setSingleCardSelected(
  taskLists: TaskBoardTaskList[], 
  targetId: number, 
  newSelectedValue: boolean): TaskBoardTaskList[] {
  const newTaskLists = [...taskLists]
  for (let row of newTaskLists) {
    if (row.taskListId === targetId) {
      row.isSelected = newSelectedValue
    }
  }
  return newTaskLists
}

export function getSelectedTaskListIds(taskLists: TaskBoardTaskList[]): Set<number> {
  const selectedIds = new Set<number>([])
  for (let row of taskLists) {
    if (row.isSelected) selectedIds.add(row.taskListId)
  }
  return selectedIds
}

export function isSomeSelected(taskLists: TaskBoardTaskList[]) {
  return taskLists.reduce((isSomeSelected, current) => {
    return isSomeSelected || current.isSelected
  }, false)
}