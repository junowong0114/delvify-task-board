// components
import { useState } from 'react'
import { InputGroup } from 'react-bootstrap'

// style
import './TaskRow.scss'

// types
import { Task } from '../../../helpers/types'

type Props = {
  onCheck: (checked: boolean) => void,
  data: Task
}

const TaskRow: React.FC<Props> = (props) => {
  const [checked, setChecked] = useState<boolean>(props.data.completed)

  function onCheck() {
    props.onCheck(!checked)
    setChecked(!checked)
  }

  return (
    <div className="TaskRow">
      <div className="slot start">
        <p className="task-row-name">{props.data.taskName}</p>
        <p className="task-row-description">{props.data.description}</p>
      </div>

      <div className="slot end">
        <InputGroup.Checkbox
          aria-label="Checkbox for task row"
          checked={checked}
          onChange={onCheck} />
      </div>
    </div>
  )
}

export default TaskRow
