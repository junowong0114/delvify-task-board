// components
import { InputGroup } from 'react-bootstrap'
import { FaPen, FaTrash } from 'react-icons/fa'
import CustomButton from '../../../components/CustomButton'
import TaskRow from '../components/TaskRow'

// Redux
import { useSelector } from 'react-redux'
import { RootState } from '../../../redux/state'

// hooks and helpers
import { useHover } from 'react-use'
import { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { getAPIServer } from '../../../helpers/api'
import { errorToString } from 'shared'

// style
import "./ListCard.scss"

// types
import { TaskList } from "../../../helpers/types"
import { API } from 'shared'

type Props = {
  data: TaskList & { isSelected: boolean }
  isEditing: boolean,
  onSelect: (action: 'select' | 'unselect') => void,
  onDelete: () => void,
  onDeleteTask: () => void,
  onError: (msg: string) => void,
}

const ListCard: React.FC<Props> = (props) => {
  const [checked, setChecked] = useState<boolean>(false)
  const token = useSelector((state: RootState) => state.auth.token)
  const history = useHistory()

  useEffect(() => {
    setChecked(props.data.isSelected)
  }, [props.data.isSelected])

  function onCheck() {
    props.onSelect(checked ? 'unselect' : 'select')
    setChecked(!checked)
  }

  function onTaskCheck(taskId: number, checked: boolean) {
    let reqBody: API.PutTasksBody = {
      tasks: [{
        taskId,
        completed: checked,
      }]
    }

    const origin = getAPIServer()
    fetch(`${origin}/tasks`, {
      method: "PUT",
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(reqBody)
    })
    .then(res => {
      return res.json() as Promise<API.PutResponse>
    })
    .then(res => {
      if ('error' in res) throw new Error(errorToString(res.error))
      // TODO: give response to user for successful complete
      props.onDeleteTask()
    })
    .catch(e => {
      props.onError(errorToString(e))
    })
  }

  const elem = (hovered: boolean) => {
    return (
      <div className="ListCard">
        <div className="list-card-header">
          <div className="buttons">
            <div className="slot start">
              <InputGroup.Checkbox
                aria-label="Checkbox for list card"
                hidden={!hovered && !props.isEditing}
                checked={checked}
                onChange={() => onCheck()} />
            </div>
            <div className="slot end">
              <CustomButton onClick={() => { history.push(`/task_list/edit/${props.data.taskListId}`) }} icon={<FaPen />} type={'secondaryLight'} />
              <CustomButton onClick={() => props.onDelete()} icon={<FaTrash />} type={'secondaryLight'} />
            </div>
          </div>
          <h3 className={hovered ? "hovered" : ""}>{props.data.taskListName}</h3>
        </div>
        <div className="list-card-content">
          {props.data.tasks.map(task => {
            return (
              <TaskRow
                key={task.taskId}
                data={task}
                onCheck={(checked: boolean) => onTaskCheck(task.taskId, checked)} />
            )
          })}
        </div>
      </div>
    )
  }

  const [hoverable] = useHover(elem)

  return (
    <>
      {hoverable}
    </>
  )
}

export default ListCard
