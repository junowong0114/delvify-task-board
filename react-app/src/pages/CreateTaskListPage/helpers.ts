import { Task, TaskList} from '../../helpers/types'
import { API, DTO } from 'shared'

export type CreatePageTask = Omit<Task, 'completed'>

export type CreatePageTaskList = Omit<TaskList, 'ownerId' | 'taskListId' | 'tasks'> & {
  tasks: CreatePageTask[]
}

export function formatCreateTaskListData(raw: CreatePageTaskList): API.PostTaskListsBody['taskLists'][number] {
  return {
    taskListName: raw.taskListName,
    tasks: raw.tasks.map(row => {
      return {
        taskName: row.taskName,
        description: row.description,
        deadline: row.deadline.toISOString(),
      }
    })
  }
}