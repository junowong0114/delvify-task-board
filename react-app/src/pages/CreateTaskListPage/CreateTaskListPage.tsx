// component
import { Button, Container, Form, InputGroup, Modal } from 'react-bootstrap'
import BackButton from '../../components/BackButton'
import TaskInputRow from '../../components/TaskInputRow'
import CustomButton from '../../components/CustomButton'
import { IoCaretForward, IoAddOutline } from 'react-icons/io5'
import { FaTrash } from 'react-icons/fa'

// redux
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/state'

// hooks and helpers
import { useHistory } from 'react-router'
import { useEffect, useState } from 'react'
import { getAPIServer } from '../../helpers/api'
import { formatCreateTaskListData } from './helpers'
import { CreatePageTask, CreatePageTaskList } from './helpers'
import { roundTime } from '../../helpers/format'

// style
import './CreateTaskListPage.scss'

// types
import { API, errorToString } from 'shared'
import { Task } from '../../helpers/types'

const CreateTaskListPage: React.FC = () => {
  const history = useHistory()
  const [data, setData] = useState<CreatePageTaskList>({
    taskListName: '',
    tasks: []
  })
  const [error, setError] = useState<string | undefined>()
  const token = useSelector((state: RootState) => state.auth.token)
  const [nextTaskId, setNextTaskId] = useState(-1)
  const [selectedTaskIds, setSelectedTaskIds] = useState<number[]>([])
  const [showModal, setShowModal] = useState<boolean>(false)

  // handle error 
  useEffect(() => {
    if (error) console.error(error)
  }, [error])

  // handle add task logic
  function onAddTaskClick() {
    const emptyTask: CreatePageTask = {
      taskId: nextTaskId,
      taskListId: -1,
      taskName: '',
      description: '',
      deadline: roundTime(new Date())
    }

    const newData = {
      ...data,
      tasks: [
        ...data.tasks,
        emptyTask
      ]
    }

    setNextTaskId(nextTaskId - 1)
    setData(newData)
  }

  // handle task change logic
  function onTaskChange(newTask: Task) {
    setData({
      ...data,
      tasks: data.tasks.map(row => {
        if (row.taskId === newTask.taskId) {
          return newTask
        }
        return row
      })
    })
  }

  // handle task row selected
  function onTaskRowSelect(taskId: number, wasSelected: boolean) {
    try {
      if (wasSelected) {
        setSelectedTaskIds(selectedTaskIds.filter(id => id !== taskId))
      } else {
        setSelectedTaskIds([
          ...selectedTaskIds,
          taskId,
        ])
      }
    } catch (e) {
      setError(errorToString(e))
    }
  }

  // handle select all checkbox clicked 
  function onSelectAllClick() {
    if (selectedTaskIds.length) {
      setSelectedTaskIds([])
    } else {
      setSelectedTaskIds(data.tasks.map(row => row.taskId))
    }
  }

  // handle delete single row
  function onDeleteSingleTask(taskId: number) {
    setData({
      ...data,
      tasks: data.tasks.filter(row => row.taskId !== taskId)
    })

    setSelectedTaskIds(selectedTaskIds.filter(id => id !== taskId))
  }

  // handle batch delete
  function onBatchDeleteTasks() {
    setData({
      ...data,
      tasks: data.tasks.filter(row => !selectedTaskIds.includes(row.taskId))
    })

    setSelectedTaskIds([])
  }

  // handle save button clicked 
  function onSaveBtnClicked() {
    // check input
    if (!data.taskListName) return setError('Task list name must not be empty.')
    if (data.tasks.some(row => row.taskName.length === 0)) return setError('Task name must not be empty.')

    // invoke API
    const origin = getAPIServer()
    const body: API.PostTaskListsBody = {
      taskLists: [formatCreateTaskListData(data)]
    }
    fetch(`${origin}/task_lists`, {
      method: "POST",
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body)
    }).then(res => {
      return res.json() as Promise<API.PostResponse>
    }).then(res => {
      if ('error' in res) throw new Error(res.error)
      // TODO: give response to user for successful task list creation
      history.push('/tabs/dash_board')
    }).catch(e => {
      setError(errorToString(e))
    })
  }

  return (
    <Container fluid="xl" className="CreateTaskListPage">
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>All unsaved changes will be lost, are you sure?</Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => setShowModal(false)}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => history.goBack()}>
            Yes
          </Button>
        </Modal.Footer>
      </Modal>

      <div className="task-list-header">
        <div className="slot start">
          <BackButton
            onClick={() => setShowModal(true)} />
          <h2>Create Task List</h2>
        </div>
        <div className="slot end">
          <button
            className="save-btn"
            onClick={onSaveBtnClicked} >
            <span>SAVE</span>
            <IoCaretForward />
          </button>
        </div>
      </div>
      <div className="task-list-content">
        <Form className="form">
          <div className="error-text">
            {error}
          </div>
          <Form.Group className="mb-3" controlId="formGroupTitle">
            <Form.Label>Task list name</Form.Label>
            <Form.Control type="text"
              placeholder="Enter task list name"
              className="task-list-name"
              value={data?.taskListName}
              onChange={e => setData({ ...data, taskListName: e.target.value })} />
          </Form.Group>

          <Form.Group>
            <Form.Label style={{ marginBottom: '0' }}>Tasks</Form.Label>
          </Form.Group>
          <div className="btn-row">
            <InputGroup.Checkbox
              aria-label="Checkbox for select all tasks"
              checked={selectedTaskIds.length}
              onChange={onSelectAllClick}
              disabled={!data.tasks.length} />
            <CustomButton
              icon={<FaTrash />}
              text='Delete'
              onClick={() => onBatchDeleteTasks()}
              type={'secondary'} />
          </div>
          <div className="tasks-container">
            {data.tasks.map(row => {
              return <TaskInputRow
                key={row.taskId}
                data={row}
                onChange={(updatedTask: Task) => onTaskChange(updatedTask)}
                onDelete={() => onDeleteSingleTask(row.taskId)}
                onSelect={(wasSelected) => onTaskRowSelect(row.taskId, wasSelected)}
                isSelected={selectedTaskIds.some(id => id === row.taskId)} />
            })}
            <div className="plus-btn-container">
              <CustomButton
                icon={<IoAddOutline />}
                text="Add a task"
                onClick={() => onAddTaskClick()}
                type={'secondary'} />
            </div>
          </div>
        </Form>
      </div>
    </Container>
  )
}

export default CreateTaskListPage
