// components
import { Button, Container, Form } from 'react-bootstrap'

// hooks and helpers
import { useHistory } from 'react-router'
import { useEffect, useState } from 'react'

// redux
import { loginWithPassWordThunk } from '../../redux/auth/thunk'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/state'

// style
import './LoginPage.scss'
import { Link } from 'react-router-dom'

const LoginPage: React.FC = () => {
  const [email, setEmail] = useState({ value: '' })
  const [password, setPassword] = useState({ value: '' })
  const [error, setError] = useState<string>()
  const history = useHistory()
  const dispatch = useDispatch()
  const [token, loginError] = useSelector((state: RootState) => {
    return [state.auth.token, state.auth.error]
  })

  // redirect to front page if token is found
  useEffect(() => {
    if (token) history.push('/dash_board')
  }, [token])

  // show error if login failed
  useEffect(() => {
    if (!loginError) return
    setError(loginError)
  }, [loginError])

  function onLoginBtnClick(e: any) {
    e.preventDefault()
    if (!email.value.length) return setError('Email address cannot be empty.')
    if (!password.value.length) return setError('Password cannot be empty.')
    dispatch(loginWithPassWordThunk(email.value, password.value))
  }

  return (
    <div className="LoginPage">
      <Container fluid>
        <h2>Login</h2>
        <main className="main-hero">
          <Form className="login-form">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email.value}
                onChange={e => setEmail({ value: e.target.value })} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password.value}
                onChange={e => setPassword({ value: e.target.value })} />
            </Form.Group>

            <Button variant="primary" type="submit" onClick={e => onLoginBtnClick(e)}>
              Sign in
            </Button>
          </Form>

          <div className="sign-up">
            New to TaskBoard? <Link to="/tabs/register">Register an account.</Link>
          </div>

          <div className="error-msg">
            {error}
          </div>
        </main>
      </Container>
    </div>
  )
}

export default LoginPage
