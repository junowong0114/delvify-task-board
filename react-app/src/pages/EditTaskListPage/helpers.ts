import { API, DTO } from 'shared'
import { TaskList } from '../../helpers/types'

// Need try-catching
export function compareEditTaskListData(original: DTO.TaskList, submitted: TaskList) {
  /* convert Date object (submitted.tasks[number].deadline) to ISOstring */
  const _submitted = formatTaskListToDtoTaskList(submitted)

  const difference: API.PutTaskListBody['taskList'] = {
    taskListId: original.taskListId,
    tasks: []
  }

  // if task list has a different name
  if (original.taskListName !== submitted.taskListName) difference.taskListName = submitted.taskListName

  // variables for store new tasks / if tasks are deleted
  let tasksToBeInserted: API.PostTasksBody['tasks'] = []
  const isTasksToBeDeleted = Object.fromEntries(original.tasks.map(row => [row.taskId, true]))

  // compute difference, tasksToBeInserted, and taskIdsToBeDeleted
  for (let submittedTask of _submitted.tasks) {
    // push task to tasksToBeInserted if it is not found in original tasks
    const originalTask = original.tasks.find(row => row.taskId === submittedTask.taskId)
    if (!originalTask) {
      if (submittedTask.taskName.length === 0) throw new Error('Missing task name.')
      if (!submittedTask.deadline) throw new Error('Missing deadline.')
      tasksToBeInserted.push({
        taskName: submittedTask.taskName,
        description: submittedTask.description,
        deadline: submittedTask.deadline,
      })
      continue
    }

    // the current task is present in both original and submitted data
    isTasksToBeDeleted[submittedTask.taskId] = false

    // compare original task and submitted task
    let k: keyof DTO.Task | 'taskListId'
    let diff: Partial<Record<typeof k, any>> & { taskId: number } = {
      taskId: submittedTask.taskId
    }
    for (k in submittedTask) {
      // special case for deadline, because iso string varies a little bit
      if (k === 'deadline') {
        const originalDeadline = (new Date(originalTask[k])).getTime()
        const submittedDeadline = (new Date(submittedTask[k])).getTime()

        if (originalDeadline === submittedDeadline) continue
        diff[k] = submittedTask[k]
        continue
      }
      if (k === 'taskListId') {
        if (submittedTask.taskListId !== original.taskListId) {
          diff[k] = submittedTask.taskListId
        }
        continue
      }
      if (originalTask[k] !== submittedTask[k]) diff[k] = submittedTask[k]
    }
    if (Object.keys(diff).length > 1) difference.tasks?.push(diff)
  }

  // compute ids of tasks to be deleted
  const taskIdsToBeDeleted: API.DeleteTasksBody['taskIds'] =
    Object.entries(isTasksToBeDeleted)
      .reduce<API.DeleteTasksBody['taskIds']>((result, current) => {
        // skip if current is not to be deleted
        if (!current[1]) return result
        return [
          ...result,
          parseInt(current[0])
        ]
      }, [])

  // return undefined if no difference in the tasks is found
  if (!difference.tasks?.length && Object.keys(difference).length === 2) {
    return [undefined, tasksToBeInserted, taskIdsToBeDeleted] as const
  }

  // if tasks is empty
  if (!difference.tasks?.length) {
    delete difference.tasks
  }

  return [difference, tasksToBeInserted, taskIdsToBeDeleted] as const
}

function formatTaskListToDtoTaskList(taskList: TaskList) {
  return {
    ...taskList,
    tasks: taskList.tasks.map(row => {
      return {
        ...row,
        deadline: row.deadline.toISOString()
      }
    })
  }
}