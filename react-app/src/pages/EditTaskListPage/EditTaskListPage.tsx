// component
import { Button, Container, Dropdown, Form, InputGroup, Modal } from 'react-bootstrap'
import BackButton from '../../components/BackButton'
import TaskInputRow from '../../components/TaskInputRow'
import CustomButton from '../../components/CustomButton'
import CustomToggle from '../../components/CustomToggle'
import { IoCaretForward, IoAddOutline } from 'react-icons/io5'
import { FaTrash } from 'react-icons/fa'
import { MdDriveFileMove } from 'react-icons/md'

// redux
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/state'

// hooks and helpers
import { useEffect, useState } from 'react'
import { useGet } from '../../helpers/hooks'
import { useHistory, useParams } from 'react-router-dom'
import { formatTaskList } from '../../helpers/TaskListFormat'
import { getAPIServer } from '../../helpers/api'
import { compareEditTaskListData } from './helpers'

// style
import './EditTaskListPage.scss'

// types
import { API, DTO, errorToString } from 'shared'
import { Task, TaskBoardTaskList, TaskList } from '../../helpers/types'
import { roundTime } from '../../helpers/format'
import useStorageState from 'react-use-storage-state'

type Params = {
  taskListId?: string
}

const TaskListEditPage: React.FC = () => {
  // TODO: replace mockAPI with real API
  const origin = getAPIServer()
  const history = useHistory()
  const { taskListId } = useParams<Params>()
  const [fetchNow, rawData, loading] = useGet<DTO.TaskList>(`${origin}/task_list`)
  const [data, setData] = useState<TaskList>({
    ownerId: 0,
    taskListId: 0,
    taskListName: '',
    tasks: []
  })
  const [error, setError] = useState<string | undefined>()
  const token = useSelector((state: RootState) => state.auth.token)
  const [nextTaskId, setNextTaskId] = useState(-1)
  const [selectedTaskIds, setSelectedTaskIds] = useState<number[]>([])
  const [taskLists] = useStorageState<TaskBoardTaskList[]>('taskLists', [])
  const [showModal, setShowModal] = useState<boolean>(false)

  // fire fetchNow API on component mount
  useEffect(() => {
    if (!token || !taskListId) return
    fetchNow(token, [taskListId])
  }, [token, taskListId])

  // call setError or setTaskLists according to fetchNow result
  useEffect(() => {
    if (loading || !rawData) return
    if ('error' in rawData) return setError(rawData.error)
    try {
      if ('data' in rawData) {
        setData(formatTaskList(rawData.data))
      }
    } catch (e) {
      setError(errorToString(e))
    }
  }, [loading, rawData])

  // handle error 
  useEffect(() => {
    if (error) console.error(error)
  }, [error])

  // handle back button pressed
  function onBackBtnClick() {
    try {
      if (!rawData || 'error' in rawData) throw new Error()
      const [difference, tasksToBeInserted, taskIdsToBeDeleted] = compareEditTaskListData(rawData.data, data)
      if (difference || tasksToBeInserted.length || taskIdsToBeDeleted.length) {
        setShowModal(true)
      } else {
        history.goBack()
      }
    } catch (e) {
      setError(errorToString(e))
      history.goBack()
    }
  }

  // handle add task logic
  function onAddTaskClick() {
    const emptyTask: Task = {
      taskId: nextTaskId,
      taskListId: data.taskListId,
      taskName: '',
      description: '',
      completed: false,
      deadline: roundTime(new Date())
    }

    const newData = {
      ...data,
      tasks: [
        ...data.tasks,
        emptyTask
      ]
    }

    setNextTaskId(nextTaskId - 1)
    setData(newData)
  }

  // handle task change logic
  function onTaskChange(newTask: Task) {
    setData({
      ...data,
      tasks: data.tasks.map(row => {
        if (row.taskId === newTask.taskId) {
          return newTask
        }
        return row
      })
    })
  }

  // handle task row selected
  function onTaskRowSelect(taskId: number, wasSelected: boolean) {
    try {
      if (wasSelected) {
        setSelectedTaskIds(selectedTaskIds.filter(id => id !== taskId))
      } else {
        setSelectedTaskIds([
          ...selectedTaskIds,
          taskId,
        ])
      }
    } catch (e) {
      setError(errorToString(e))
    }
  }

  // handle select all checkbox clicked 
  function onSelectAllClick() {
    if (selectedTaskIds.length) {
      setSelectedTaskIds([])
    } else {
      setSelectedTaskIds(data.tasks.map(row => row.taskId))
    }
  }

  // handle delete single row
  function onDeleteSingleTask(taskId: number) {
    setData({
      ...data,
      tasks: data.tasks.filter(row => row.taskId !== taskId)
    })

    setSelectedTaskIds(selectedTaskIds.filter(id => id !== taskId))
  }

  // handle batch delete
  function onBatchDeleteTasks() {
    setData({
      ...data,
      tasks: data.tasks.filter(row => !selectedTaskIds.includes(row.taskId))
    })

    setSelectedTaskIds([])
  }
  // handle drop down item clicked
  async function onDropDownItemClick(taskListId: number) {
    if (!selectedTaskIds.length) return

    setData({
      ...data,
      tasks: data.tasks.map(row => {
        if (!selectedTaskIds.includes(row.taskId)) return row
        return {
          ...row,
          taskListId: taskListId
        }
      })
    })

    setSelectedTaskIds([])
  }

  // handle save btn clicked
  async function onSaveBtnClicked() {
    // check input
    if (!data.taskListName) return setError('Task list name must not be empty.')
    if (data.tasks.some(row => row.taskName.length === 0)) return setError('Task name must not be empty.')
    if (!rawData || 'error' in rawData) return
    try {
      const [difference, tasksToBeInserted, taskIdsToBeDeleted] = compareEditTaskListData(rawData.data, data)
      console.log(difference)
      if (difference) {
        const body: API.PutTaskListBody = {
          taskList: difference
        }
        const putTaskListRes = await fetch(`${origin}/task_list`, {
          method: "PUT",
          headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(body),
        })
        const res = await putTaskListRes.json()
        if ('error' in res) throw new Error(res.error)
      }

      if (tasksToBeInserted.length) {
        const body: API.PostTasksBody = {
          taskListId: data.taskListId,
          tasks: tasksToBeInserted
        }
        const createTasksRes = await fetch(`${origin}/tasks`, {
          method: "POST",
          headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(body),
        })
        const res = await createTasksRes.json()
        if ('error' in res) throw new Error(res.error)
      }

      if (taskIdsToBeDeleted.length) {
        const body: API.DeleteTasksBody = {
          taskIds: taskIdsToBeDeleted
        }
        const deleteTasksRes = await fetch(`${origin}/tasks`, {
          method: "DELETE",
          headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(body),
        })
        const res = await deleteTasksRes.json()
        if ('error' in res) throw new Error(res.error)
      }

      history.push('/tabs/dash_board')
    } catch (e) {
      setError(`Failed to update: ${errorToString(e)}`)
    }
  }

  return (
    <Container fluid="xl" className="TaskListEditPage">
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>All changes to the task list will be lost, are you sure?</Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => setShowModal(false)}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => history.goBack()}>
            Yes
          </Button>
        </Modal.Footer>
      </Modal>

      <div className="task-list-header">
        <div className="slot start">
          <BackButton 
            onClick={onBackBtnClick} />
          <h2>Edit Task List</h2>
        </div>
        <div className="slot end">
          <button
            className="save-btn"
            onClick={onSaveBtnClicked} >
            <span>SAVE</span>
            <IoCaretForward />
          </button>
        </div>
      </div>
      <div className="task-list-content">
        <Form className="form">
          <div className="error-text">
            {error}
          </div>
          <Form.Group className="mb-3" controlId="formGroupTitle">
            <Form.Label>Task list name</Form.Label>
            <Form.Control type="text"
              placeholder="Enter task list name"
              className="task-list-name"
              value={data?.taskListName}
              onChange={e => setData({ ...data, taskListName: e.target.value })} />
          </Form.Group>

          <Form.Group>
            <Form.Label style={{ marginBottom: '0' }}>Tasks</Form.Label>
          </Form.Group>
          <div className="btn-row">
            <InputGroup.Checkbox
              aria-label="Checkbox for select all tasks"
              checked={selectedTaskIds.length}
              onChange={onSelectAllClick}
              disabled={!data.tasks.length} />
            <Dropdown>
              <Dropdown.Toggle as={CustomToggle}>
                <CustomButton
                  icon={<MdDriveFileMove style={{ width: '20px' }} />}
                  text='Move to'
                  onClick={() => { }}
                  type={'secondary'} />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                {taskLists
                  .filter(row => row.taskListId !== data.taskListId)
                  .map(row => {
                    return (
                      <Dropdown.Item
                        key={row.taskListId}
                        onClick={() => onDropDownItemClick(row.taskListId)}>
                        {row.taskListName}
                      </Dropdown.Item>)
                  })}
              </Dropdown.Menu>
            </Dropdown>
            <div className="backdrop">
              <CustomButton
                icon={<FaTrash />}
                text='Delete'
                onClick={() => onBatchDeleteTasks()}
                type={'secondary'} />
            </div>
          </div>
          <div className="tasks-container">
            {data.tasks
              .filter(row => row.taskListId === data.taskListId)
              .map(row => {
                return <TaskInputRow
                  key={row.taskId}
                  data={row}
                  onChange={(updatedTask: Task) => onTaskChange(updatedTask)}
                  onDelete={() => onDeleteSingleTask(row.taskId)}
                  onSelect={(wasSelected) => onTaskRowSelect(row.taskId, wasSelected)}
                  isSelected={selectedTaskIds.some(id => id === row.taskId)} />
              })}
            <div className="plus-btn-container">
              <CustomButton
                icon={<IoAddOutline />}
                text="Add a task"
                onClick={() => onAddTaskClick()}
                type={'secondary'} />
            </div>
          </div>
        </Form>
      </div>
    </Container>
  )
}

export default TaskListEditPage
