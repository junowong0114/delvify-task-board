import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { history } from './history'
import { RootState } from './state'
import { RootAction } from './action'
import { authReducer } from './auth/reducer'

export const rootReducer: (
  state: RootState | undefined,
  action: RootAction,
) => RootState = combineReducers<RootState>({
  router: connectRouter(history),
  auth: authReducer,
})