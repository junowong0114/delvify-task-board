import { CallHistoryMethodAction } from 'connected-react-router'
import { AuthAction } from './auth/action'

export type RootAction = 
  | CallHistoryMethodAction
  | AuthAction