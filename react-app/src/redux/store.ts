import { createStore, applyMiddleware, compose } from "redux"
import { createLogger } from "redux-logger"
import thunk from "redux-thunk"
import { rootReducer } from "./reducer"
import { routerMiddleware } from "connected-react-router"
import { history } from "./history"

import dotenv from 'dotenv'

dotenv.config()

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

// add redux logger to middleware in development environment
let middleware = [
  thunk,
  routerMiddleware(history)
]
if (process.env.NODE_ENV === "development") {
  const logger = createLogger({
    collapsed: true,
  })
  middleware = [...middleware, logger]
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const rootEnhancer = composeEnhancers(
  applyMiddleware(...middleware),
)

const store = createStore(rootReducer, rootEnhancer)

export default store