import { AuthState, initialState } from './state'
import { AuthAction } from './action'

export const authReducer = (
  state: AuthState = initialState,
  action: AuthAction
): AuthState => {
  switch(action.type) {
    case '@Auth/logout':
      return {
        ...state,
        user: undefined,
        token: undefined,
        error: undefined,
        loading: false
      }

    case '@Auth/loginSuccess':
      return {
        ...state,
        user: action.user,
        token: action.token,
        error: undefined,
        loading: false,
      }

    case '@Auth/loginFailed':
      return {
        ...state,
        error: action.error,
        loading: false,
      }

    case '@Auth/loginLoading':
      return {
        ...state,
        error: undefined,
        loading: true,
      }

    default:
      return state
  }
}
