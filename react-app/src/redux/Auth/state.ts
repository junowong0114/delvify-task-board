import { JWTPayload } from "shared"

export type AuthState = {
  user?: JWTPayload,
  token?: string,
  error?: string,
  loading: boolean,
}

export const initialState: AuthState = {
  user: undefined,
  token: undefined,
  error: undefined,
  loading: false,
}
