import { JWTPayload } from "shared"

export function logout() {
  return {
    type: '@Auth/logout' as const,
  }
}

export function loginSuccess(token: string, user: JWTPayload) {
  return {
    type: '@Auth/loginSuccess' as const,
    user,
    token,
  }
}

export function loginFailed(error: string) {
  return {
    type: '@Auth/loginFailed' as const,
    error,
  }
}

export function loginLoading() {
  return {
    type: '@Auth/loginLoading' as const,
    loading: true,
  }
}

export type AuthAction = 
  | ReturnType<typeof logout>
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loginFailed>
  | ReturnType<typeof loginLoading>
