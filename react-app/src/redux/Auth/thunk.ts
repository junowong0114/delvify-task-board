import { API, errorToString, JWTPayload } from 'shared'
import { getAPIServer } from '../../helpers/api'
import { RootDispatch } from '../dispatch'
import { loginFailed, loginSuccess, logout } from './action'
import jwtDecode from 'jwt-decode'

export function loginWithPassWordThunk(email: string, password: string) {
  return async (dispatch: RootDispatch) => {
    let origin: string
    let json: API.PostLoginResponse

    try {
      origin = getAPIServer()
      const reqPayload: API.PostLoginBody = {
        email,
        password,
      }

      const res = await fetch(`${origin}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(reqPayload)
      })
      json = await res.json()
      if ('error' in json) throw new Error(json.error)

      dispatch(handleTokenThunk(json.token))
    } catch (e) {
      dispatch(loginFailed(errorToString(e)))
    }
  }
}

export function loginWithTokenThunk(token: string) {
  return (dispatch: RootDispatch) => {
    dispatch(handleTokenThunk(token))
  }
}

function handleTokenThunk(token: string) {
  return (dispatch: RootDispatch) => {
    localStorage.setItem('taskBoardToken', token)
    try {
      const payload = jwtDecode<JWTPayload>(token)
      dispatch(loginSuccess(token, payload))
    } catch (e) {
      dispatch(loginFailed(errorToString(e)))
    }
  }
}

export function checkTokenThunk() {
  return (dispatch: RootDispatch) => {
    const token = localStorage.getItem('taskBoardToken')
    if (!token) return dispatch(logout())
    dispatch(handleTokenThunk(token))
  }
}

export function logoutThunk() {
  return (dispatch: RootDispatch) => {
    localStorage.removeItem('taskBoardToken')
    dispatch(logout())
  }
}