// redux related
import { useDispatch } from 'react-redux'
import { checkTokenThunk } from './redux/auth/thunk';

// react router related
import { ConnectedRouter } from 'connected-react-router';
import { history } from './redux/history';
import { Redirect, Route, Switch } from 'react-router-dom';

// style
import "./App.scss"

// components
import TaskBoardPage from './pages/TaskBoardPage/TaskBoardPage'
import EditTaskListPage from './pages/EditTaskListPage/EditTaskListPage'
import CreateTaskListPage from './pages/CreateTaskListPage/CreateTaskListPage'
import LoginPage from './pages/LoginPage/LoginPage'
import RegisterPage from './pages/RegisterPage/RegisterPage'
import SideBar from './components/SideBar'
import { HiMenu } from 'react-icons/hi'
import { Button } from 'react-bootstrap';

// hooks
import { useGuard, useWindowSize } from './helpers/hooks'
import { useEffect, useState } from 'react';

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(checkTokenThunk())
    if (!localStorage.getItem('taskBoardToken')) {
      history.push('/tabs/login')
    }
  }, [])

  return (
    <ConnectedRouter history={history}>
      <div className="App">
        <Switch>
          <Route path="/tabs" component={TabApp}></Route>
          <Route path="/task_list/edit/:taskListId" component={EditTaskListPage}></Route>
          <Route path="/task_list/create" component={CreateTaskListPage}></Route>
          <Route path="/">
            <Redirect to="/tabs/dash_board" />
          </Route>
        </Switch>
      </div>
    </ConnectedRouter>
  )
}

const TabApp: React.FC = () => {
  useGuard()
  const [width] = useWindowSize()
  const isToggleBtnHidden = width > 992
  const [isSidebarToggled, setSidebarToggled] = useState<boolean>(false)

  return (
    <div className="App tab-app">
      <SideBar
        toggled={isSidebarToggled}
        onToggle={() => setSidebarToggled(false)}
      />
      <main className="page-app">
        <div className="top-bar" hidden={isToggleBtnHidden}>
          <Button
            variant="toggleSidebar"
            onClick={() => setSidebarToggled(true)}
          >
            <HiMenu />
          </Button>
        </div>
        <Switch>
          <Route path="/tabs/dash_board" exact component={TaskBoardPage}></Route>
          <Route path="/tabs/login" exact component={LoginPage}></Route>
          <Route path="/tabs/register" exact component={RegisterPage}></Route>
        </Switch>
      </main>
    </div>
  )
}

export default App
